# Egile-eskubideak

Dokumentu honetan, **Bertsotan Android** aplikazioaren iturburu-kodea osatzen duten fitxategien lizentziak zehazten dira.

## Proiektua

Proiektu hau [software librea](https://eu.wikipedia.org/wiki/Software_libre) da.

Aplikazio honen fitxategi gehienak GNUren **Affero Lizentzia Publiko Orokorra**ren 3. bertsiopean daude ([AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.txt) lizentzia).

## Osagaiak

Ondoko fitxategiak ere **libreak dira** baina egile edo lizentzia desberdinak dituzte:

- `/app/libs/libstt-1.1.0-release.aar` liburutegia [Coqui STT](https://github.com/coqui-ai/STT) proiektukoa da eta Mozillaren Lizentzia Publikoaren 2.0 bertsiopean dago ([MPL 2.0](http://mozilla.org/MPL/2.0/) lizentzia).
- `/app/src/main/res/drawable/atzeko_berdea.jpg` irudia Chenspec-en [irudi batean oinarrituta](https://pixabay.com/illustrations/woman-brain-mindset-emotion-mood-6001298/) dago eta jabetza publikoan dago ([Pixabay](https://pixabay.com/service/terms/#license) lizentzia).
- `/app/src/main/res/drawable/ikonoa_*.xml` ikonoak [Material Design Ikonoak](https://github.com/google/material-design-icons) proiektuko irudietan oinarrituta daude eta [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt) lizentziapean daude.
- `/app/src/main/res/raw/errimategia.json` fitxategia [Bertsotan Errimategia](https://gitlab.com/bertsotan/errimategia) proiektukoa da eta jabetza publikoan dago ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.eu) eskaintza).
- `/app/src/main/res/raw/kenlm.scorer` fitxategia [Francis Tyers](https://cl.indiana.edu/~ftyers/)ek sortua da eta jabetza publikoan dago ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.eu) eskaintza).
- `/app/src/main/res/raw/model.tflite` fitxategia [Xabier de Zuazo](https://github.com/zuazo)k sortua da eta GNUren Affero Lizentzia Publiko Orokorraren 3. bertsiopean dago ([AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.txt) lizentzia).
  - Hizkuntza eredu hau sortzeko Mozillaren Common Voice proiektuaren [euskarazko 12.0 datu-sorta](https://commonvoice.mozilla.org/eu/datasets) erabili da eta jabetza publikoan dago ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.eu) eskaintza). 