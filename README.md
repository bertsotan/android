# Bertsotan Android

Android aplikazio hau [**Bertsotan**](https://gitlab.com/bertsotan) proiektuaren parte da. Aplikazio honen helburua bertsotan egiten laguntzea da. Horretarako hainbat funtzionalitate lagungarriak izan badaitezke ere, oraingoz errimak bilatzearena bakarrik dago garatuta.  

Funtzionalitateak:

- **Errimak bilatu.** Bertsoen oinak aurkitzen eta aukeratzen laguntzen du, bilatutako oinarekin hobeto errimatzen dutenak proposatuz. Horretarako, [**Bertsotan Errimategia**](https://gitlab.com/bertsotan/errimategia) erabiltzen du. 
- **Hizketaren ezagutza.** Testua sartzeko teklatua erabili ordez, gailuaren mikrofonoa baliatuz, ahotsa erabiltzeko aukera ematen du. Horretarako, [hizkuntzaren prozesamendua](https://eu.wikipedia.org/wiki/Hizkuntzaren_prozesamendu) ahalbidetzen duten [machine learning](https://eu.wikipedia.org/wiki/Ikasketa_automatiko) metodoak erabiltzen ditu. 

## Instalatu

Oraindik aplikazio hau ez dago Aplikazioen Biltegietan instalagarri. Barkatu eragozpenak!

## Ekarpenak

Proiektu honek izaera parte-hartzailea du eta ekarpenak ongi etorriak dira.

Mesedez, iturburu-kodean egin nahi dituzun ekarpenak Git proiektu honetantxe oinarritu: https://gitlab.com/bertsotan/android/ Ondoren, egin *Merge Request* bat proposatu nahi dituzun aldaketak azalduz.

Android aplikazio hau **Kotlin** programazio lengoaian garatuta dago. Garapen ingurunea prestatzeko ondokoak behar dituzu:

- Java SDK
- Android Studio
- Git

## Lizentziak

Proiektu hau [software librea](https://eu.wikipedia.org/wiki/Software_libre) da.

Egile-eskubideei dagozkien azalpenak [COPYRIGHT](https://gitlab.com/bertsotan/android/-/blob/main/COPYRIGHT.md) dokumentuan daude.

## Harremanetarako

Aplikazioan ekarpen bat egin nahi baduzu, mesedez sortu ezazu [intzidentzia](https://gitlab.com/bertsotan/android/-/issues) berri bat. Bestelako kontuetarako [bertsotan@ikusimakusi.eus](mailto:bertsotan@ikusimakusi.eus) helbidera idatz dezakezu.

## Esker bereziak

Proiektu hau ez da Marko Txopiteak bakarka sortutako zerbait, ezta gutxiago ere. Zorionez, laguntza asko jaso dut bidean eta guztiei eskerrak eman nahi dizkiet:
- Mozillaren [Common Voice](https://commonvoice.mozilla.org/eu) proiektu libre eta parte-hartzaileko kideak. Bereziki euskara atalekoak: [Librezale](https://librezale.eus/) eta [beste guztiak](https://librezale.eus/wiki/CommonVoice#Eskerrak).
- [Coqui](https://coqui.ai/) proiektu libre eta parte-hartzaileko kideak. Bereziki euskarazko eredu librea sortu duenari: [Xabier de Zuazo](https://github.com/zuazo).
- Barakaldoko **Kilika** bertso-eskolako [Ander Ezeiza](https://bdb.bertsozale.eus/web/haitzondo/view/4520-ander-ezeiza) nire irakaslea eta ikaskideak: *Ibon Artorkitza*, *Marta Goikoetxea* eta abar.
- Eta gehiago: [Idoia Anzorandia](https://bdb.bertsozale.eus/web/haitzondo/view/3226-idoia-anzorandia), [Ana Picallo](https://bdb.bertsozale.eus/web/haitzondo/view/4589-ana-pikallo), [Maialen Akarregi](https://bdb.bertsozale.eus/web/haitzondo/view/3203-maialen-akarregi) eta modu batera edo bestera proiektu hau egia bihurtzen lagundu duten guztiei!

*"Urrunago ikusi badut, erraldoien bizkar gainean nagoelako da."* &ndash; Isaac Newton
