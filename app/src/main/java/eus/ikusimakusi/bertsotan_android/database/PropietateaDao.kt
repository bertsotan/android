package eus.ikusimakusi.bertsotan_android.database

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface PropietateaDao {

    @Insert
    suspend fun insert(propietateaEntity: PropietateaEntity)

    @Update
    suspend fun update(propietateaEntity: PropietateaEntity)

    @Delete
    suspend fun delete(propietateaEntity: PropietateaEntity)

    @Query("SELECT * FROM `propietatea`")
    fun fetchAllPropietateak(): Flow<List<PropietateaEntity>>

    @Query("SELECT * FROM `propietatea` WHERE propietatea=:propietatea")
    fun fetchPropietateaByIzena(propietatea: String): Flow<List<PropietateaEntity>>
}