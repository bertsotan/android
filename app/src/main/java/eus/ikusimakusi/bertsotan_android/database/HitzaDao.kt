package eus.ikusimakusi.bertsotan_android.database

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface HitzaDao {

    @Insert
    suspend fun insert(hitzaEntity: HitzaEntity)

    @Update
    suspend fun update(hitzaEntity: HitzaEntity)

    @Delete
    suspend fun delete(hitzaEntity: HitzaEntity)

    @Query("SELECT * FROM `hitza` WHERE hitza=:hitza")
    fun fetchHitzaByHitza(hitza: String): Flow<List<HitzaEntity>>

    @Query("SELECT * FROM `hitza` WHERE bukaera=:bukaera")
    fun fetchHitzakByBukaera(bukaera: String): Flow<List<HitzaEntity>>

    @Query("SELECT * FROM `hitza` WHERE bukaera IN (:bukaerak)")
    fun fetchHitzakByBukaerak(bukaerak: List<String>): Flow<List<HitzaEntity>>

}