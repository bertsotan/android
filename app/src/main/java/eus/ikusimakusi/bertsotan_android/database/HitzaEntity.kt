package eus.ikusimakusi.bertsotan_android.database

import androidx.room.*

@Entity(tableName="hitza")
data class HitzaEntity(
    @PrimaryKey()
    var hitza: String = "",
    var maiuskularikDu: Boolean = false,
    var hutsunerikDu: Boolean = false,
    var ahoskera1: String = "",
    var ahoskera2: String = "",
    var bukaera: String = "",
    var erabilgarritasuna: Int = 0,
    var polisemikoaDa: Boolean = false,
    var lokalaDa: Boolean = false,
    var lagunartekoaDa: Boolean = false,
    var erderakadaDa: Boolean = false,
    var deklinatutaDago: Boolean = false,
    var iruzkina: String = ""
)
