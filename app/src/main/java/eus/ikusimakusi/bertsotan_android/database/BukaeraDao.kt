package eus.ikusimakusi.bertsotan_android.database

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface BukaeraDao {

    @Insert
    suspend fun insert(bukaeraEntity: BukaeraEntity)

    @Update
    suspend fun update(bukaeraEntity: BukaeraEntity)

    @Delete
    suspend fun delete(bukaeraEntity: BukaeraEntity)

    @Query("SELECT * FROM `bukaera`")
    fun fetchAllBukaerak(): Flow<List<BukaeraEntity>>

    @Query("SELECT * FROM `bukaera` WHERE ahoskera1 = :ahoskera1")
    fun fetchBukaerakByAhoskera1(ahoskera1: String): Flow<List<BukaeraEntity>>

    @Query("SELECT * FROM `bukaera` WHERE ahoskera2 = :ahoskera2 AND ahoskera1 != :ahoskera1")
    fun fetchBukaerakByAhoskera2EzAhoskera1(ahoskera2: String, ahoskera1: String): Flow<List<BukaeraEntity>>
}