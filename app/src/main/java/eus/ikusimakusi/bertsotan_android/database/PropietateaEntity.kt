package eus.ikusimakusi.bertsotan_android.database

import androidx.room.*

@Entity(tableName="propietatea")
data class PropietateaEntity(
    @PrimaryKey()
    var propietatea: String = "",
    var balioa: String = ""
)
