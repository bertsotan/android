package eus.ikusimakusi.bertsotan_android.database

import androidx.room.*

@Entity(tableName="bukaera")
data class BukaeraEntity(
    @PrimaryKey()
    var ahoskera1: String = "",
    var ahoskera2: String = ""
)
