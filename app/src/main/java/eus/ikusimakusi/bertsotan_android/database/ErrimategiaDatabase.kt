package eus.ikusimakusi.bertsotan_android.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [PropietateaEntity::class, BukaeraEntity::class, HitzaEntity::class], version = 1)
abstract class ErrimategiaDatabase: RoomDatabase() {

    abstract fun propietateaDao(): PropietateaDao
    abstract fun bukaeraDao(): BukaeraDao
    abstract fun hitzaDao(): HitzaDao

    companion object{
        @Volatile
        private var INSTANCE: ErrimategiaDatabase? = null

        fun getInstance(context: Context): ErrimategiaDatabase{
            synchronized(this){
                var instance = INSTANCE

                if(instance == null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        ErrimategiaDatabase::class.java,
                        "errimategia"
                    ).fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}