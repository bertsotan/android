package eus.ikusimakusi.bertsotan_android.adapters

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.util.TypedValue
import android.view.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import eus.ikusimakusi.bertsotan_android.R
import eus.ikusimakusi.bertsotan_android.databinding.LerroaBilatuErrimakBinding
import eus.ikusimakusi.bertsotan_android.models.HitzaZerrendan
import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak
import eus.ikusimakusi.bertsotan_android.tresnak.Tresnak
import java.util.*

class HitzaAdapter(private val zerrendaMota: String,
        private val hitzZerrenda: ArrayList<HitzaZerrendan>,
        private val aukeratuListener: (posizioa: Int, hitza: HitzaZerrendan) -> Unit,
        private val ikusiListener: (posizioa: Int, hitza: HitzaZerrendan) -> Unit
    ): RecyclerView.Adapter<HitzaAdapter.ViewHolder>() {

    class ViewHolder(binding: LerroaBilatuErrimakBinding) : RecyclerView.ViewHolder(binding.root){
        val tvHitza = binding.tvHitza
        val tvPuntuak = binding.tvPuntuak
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LerroaBilatuErrimakBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val context = holder.itemView.context
        val hitza = hitzZerrenda[position]

        // Hitzaren testua
        holder.tvHitza.text = hitza.lortuHitza()
        if (hitza.lortuHitza() == ""){
            holder.tvHitza.text = (position + 1).toString()  // 3
        }else{
            if (hitza.eskuzkoaDa){
                holder.tvHitza.text = "\"${hitza.lortuHitza()}\""  // "Karina"
            }else{
                holder.tvHitza.text = hitza.lortuHitza()  // klima
            }
        }

        // Hitzaren lerrokatzea
        if (hitza.lortuHitza() == ""){
            holder.tvHitza.textAlignment = View.TEXT_ALIGNMENT_CENTER
        }else{
            holder.tvHitza.textAlignment = View.TEXT_ALIGNMENT_TEXT_START
        }
        // Hitzaren letra mota
        if (hitza.lortuHitza() == ""){
            holder.tvHitza.setTypeface(null, Typeface.ITALIC)
        }else{
            if(hitza.polisemikoaDa){
                if(hitza.lagunartekoaDa or hitza.erderakadaDa or hitza.lokalaDa){
                    holder.tvHitza.setTypeface(null, Typeface.BOLD_ITALIC)
                }else{
                    holder.tvHitza.setTypeface(null, Typeface.BOLD)
                }
            }else{
                if(hitza.lagunartekoaDa or hitza.erderakadaDa or hitza.lokalaDa){
                    holder.tvHitza.setTypeface(null, Typeface.ITALIC)
                }else{
                    holder.tvHitza.setTypeface(null, Typeface.NORMAL)
                }
            }
        }

        // Hitzaren kolorea
        if (hitza.lortuHitza() == ""){
            holder.tvHitza.setTextColor(ContextCompat.getColor(context, R.color.colorLightGray))
        }else {
            holder.tvHitza.setTextColor(ContextCompat.getColor(context, R.color.black))
        }

        // Hitzaren tamaina
        val hitzarenHitza = hitza.lortuHitza()
        if(hitzarenHitza.length < 10){
            holder.tvHitza.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
        }else if(hitzarenHitza.length < 12){
            holder.tvHitza.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)
        }else if(hitzarenHitza.length < 14){
            holder.tvHitza.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
        }else if(hitzarenHitza.length < 16){
            holder.tvHitza.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13F)
        }else{
            holder.tvHitza.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
        }

        // Puntuazioaren testua
        if (zerrendaMota == Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK){
            holder.tvPuntuak.text = ""
        }else{
            holder.tvPuntuak.text = hitza.puntuazioa.toString()
        }

        // Atzekoaren kolorea
        if (zerrendaMota == Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK){  // Hautatuak
            if (hitza.lortuHitza() == "") {
                holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_hutsik)
            }else{
                holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_gaituta)
            }
        }else{  // Errimarenak edo Familiakoak
            if(hitza.desgaitutaDago){
                holder.tvHitza.setTextColor(ContextCompat.getColor(context, R.color.black))
                holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_hutsik)
            }else{
                holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_gaituta)
            }
        }

        // Papertxoaren garaiera
        if (zerrendaMota == Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK && !hitza.hutsikDago()
            ||
            zerrendaMota != Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK && !hitza.desgaitutaDago
        ) {
            holder.itemView.elevation = Tresnak.lortuPixelakDptik(2, context)
        }else{
            holder.itemView.elevation = Tresnak.lortuPixelakDptik(0, context)
        }

        holder.tvHitza.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                if (zerrendaMota == Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK && !hitza.hutsikDago()
                    ||
                    zerrendaMota != Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK && !hitza.desgaitutaDago
                ) {
                    notifyItemChanged(holder.adapterPosition)
                    aukeratuListener.invoke(holder.adapterPosition, hitza)
                }
            }
        })

        holder.tvHitza.setOnLongClickListener(object: View.OnLongClickListener{
            override fun onLongClick(v: View?): Boolean {
                if (!hitza.hutsikDago()){
                    if (zerrendaMota != Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK && hitza.desgaitutaDago){
                        holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_desgaituta)
                    }else{
                        holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_gaituta)
                    }
                    notifyItemChanged(holder.adapterPosition)
                    ikusiListener.invoke(holder.adapterPosition, hitza)
                }
                return true
            }
        })

        holder.tvHitza.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        if (!hitza.hutsikDago())
                            holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_fokuarekin)
                    }
                    MotionEvent.ACTION_UP -> {
                        if (zerrendaMota == Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK) {
                            if (hitza.hutsikDago()){
                                holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_hutsik)
                            }else{
                                holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_gaituta)
                            }
                        }else{
                            if (hitza.desgaitutaDago){
                                holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_desgaituta)
                            }else{
                                holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_gaituta)
                            }
                        }
                    }
                    MotionEvent.ACTION_OUTSIDE -> {
                        if (hitza.hutsikDago()){
                            holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_hutsik)
                        }else if (hitza.desgaitutaDago){
                            holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_desgaituta)
                        }else{
                            holder.itemView.setBackgroundResource(R.drawable.hitza_zerrendan_gaituta)
                        }
                    }
                }
                notifyItemChanged(holder.adapterPosition)

                return v?.onTouchEvent(event) ?: true
            }
        })
    }

    override fun getItemCount(): Int {
        return hitzZerrenda.size
    }

    fun onItemMove(fromPosition: Int, toPosition: Int) {
        hitzZerrenda.add(toPosition, hitzZerrenda.removeAt(fromPosition))
        notifyItemMoved(fromPosition, toPosition)
    }

}