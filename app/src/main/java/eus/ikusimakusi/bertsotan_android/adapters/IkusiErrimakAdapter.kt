package eus.ikusimakusi.bertsotan_android.adapters

import android.util.TypedValue
import android.view.*
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import eus.ikusimakusi.bertsotan_android.databinding.LerroaIkusiErrimakBinding
import eus.ikusimakusi.bertsotan_android.models.HitzaZerrendan
import eus.ikusimakusi.bertsotan_android.tresnak.Tresnak
import java.util.*

class IkusiErrimakAdapter(private val errimenZerrenda: ArrayList<HitzaZerrendan>,
                          private val itxiHautatutakoErrimakListener: () -> Unit
    ): RecyclerView.Adapter<IkusiErrimakAdapter.ViewHolder>() {

    class ViewHolder(binding: LerroaIkusiErrimakBinding) : RecyclerView.ViewHolder(binding.root){
        val tvErrima = binding.tvErrima
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LerroaIkusiErrimakBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val context = holder.itemView.context
        val hitza = errimenZerrenda[position]

        // Errimaren testua
        holder.tvErrima.text = hitza.lortuHitza()

        // Errimaren tamaina
        var hizkiKopurua = 0
        for (hitza in errimenZerrenda){
            if (hitza.lortuHitza().length > hizkiKopurua)
                hizkiKopurua = hitza.lortuHitza().length
        }
        if(hizkiKopurua < 10){
            holder.tvErrima.setTextSize(TypedValue.COMPLEX_UNIT_SP, 38F)
        }else if(hizkiKopurua < 12){
            holder.tvErrima.setTextSize(TypedValue.COMPLEX_UNIT_SP, 36F)
        }else if(hizkiKopurua < 14){
            holder.tvErrima.setTextSize(TypedValue.COMPLEX_UNIT_SP, 34F)
        }else if(hizkiKopurua < 16){
            holder.tvErrima.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32F)
        }else{
            holder.tvErrima.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
        }

        // Errimaren marginak
        var marginaDp = 0
        when (errimenZerrenda.size){
            1, 2 -> marginaDp = 24
            3, 4 -> marginaDp = 10
            5, 6 -> marginaDp = 5
            7 -> marginaDp = 3
            8 -> marginaDp = 0
            else -> marginaDp = 0
        }
        val marginaPixel = Tresnak.lortuPixelakDptik(marginaDp, context).toInt()
        val paramMarginak = holder.tvErrima.layoutParams as LinearLayout.LayoutParams
        paramMarginak.setMargins(0, marginaPixel, 0, marginaPixel)
        holder.tvErrima.layoutParams = paramMarginak

        // Klik egiten den antzeman
        holder.itemView.setOnClickListener {
            itxiHautatutakoErrimakListener()
        }
        holder.tvErrima.setOnClickListener {
            itxiHautatutakoErrimakListener()
        }
    }

    override fun getItemCount(): Int {
        return errimenZerrenda.size
    }

}