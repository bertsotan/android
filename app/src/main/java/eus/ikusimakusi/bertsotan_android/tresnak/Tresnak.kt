package eus.ikusimakusi.bertsotan_android.tresnak

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.util.Log
import android.util.TypedValue
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import eus.ikusimakusi.bertsotan_android.R
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.math.RoundingMode
import java.text.DecimalFormat

object Tresnak {

    private lateinit var aplikazioarenEzarpenak : SharedPreferences

    fun init(context: Context) {
        // Metodo hau ErrimategiaApp sortzen denean deitzen da

        aplikazioarenEzarpenak = context.getSharedPreferences(Konstanteak.EZARPENAK_IZENA, Context.MODE_PRIVATE)
    }

    /*fun isNetworkAvailable(context: Context) : Boolean{
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Android 23 edo berriagoa
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when{
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        }else{
            // Android 22 edo zaharragoa
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo !=null && networkInfo.isConnectedOrConnecting
        }
    }
     */

    fun bihurtuBytetikMegaBytera(byteak: Int): Int{
        val df = DecimalFormat("####")  // Zenbaki osoak punturik gabe
        df.roundingMode = RoundingMode.CEILING  // Borobildu gorantz
        val megabyteak = byteak / 1024 / 1024
        return df.format(megabyteak).toInt()
    }

    fun lortuAsrMegaByteak(): Int{
        return bihurtuBytetikMegaBytera(Konstanteak.ASR_TFLITE_TAMAINA + Konstanteak.ASR_SCORER_TAMAINA)
    }

    fun lortuPixelakDptik(dpKopurua: Int, context: Context): Float{
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,dpKopurua.toFloat(),context.resources.displayMetrics
        )
    }

    private fun ezarriEzarpenaBoolean(ezarpenarenGakoa: String, balioa: Boolean){
        Log.i(this::class.simpleName, "SharedPreferences: $ezarpenarenGakoa = $balioa")
        val editor = aplikazioarenEzarpenak.edit()
        editor.putBoolean(
            ezarpenarenGakoa,
            balioa
        )
        editor.apply()
    }

    fun ezarriEzarpenakSoinua(balioa: Boolean){
        ezarriEzarpenaBoolean(Konstanteak.EZARPENAK_SOINUA, balioa)
    }

    fun ezarriEzarpenakAsr(balioa: Boolean){
        ezarriEzarpenaBoolean(Konstanteak.EZARPENAK_ASR, balioa)
    }

    private fun ezarpenaBooleanGaitutaDago(ezarpenarenGakoa: String, balioLehenetsia: Boolean): Boolean{
        if(!aplikazioarenEzarpenak.contains(ezarpenarenGakoa)){
            ezarriEzarpenaBoolean(ezarpenarenGakoa, balioLehenetsia)
            return balioLehenetsia
        }else{
            return aplikazioarenEzarpenak.getBoolean(ezarpenarenGakoa, balioLehenetsia)
        }
    }

    fun ezarpenakSoinuaGaitutaDago(): Boolean{
        return ezarpenaBooleanGaitutaDago(Konstanteak.EZARPENAK_SOINUA, Konstanteak.EZARPENAK_SOINUA_LEHENETSIA)
    }

    fun ezarpenakAsrGaitutaDago(): Boolean{
        return ezarpenaBooleanGaitutaDago(Konstanteak.EZARPENAK_ASR, Konstanteak.EZARPENAK_ASR_LEHENETSIA)
    }

    suspend fun erauziFitxategia(fromInputStream: InputStream, toFile: File){
        // fromInputStream:
        //   storage/emulated/0/Android/data/eus.ikusimakusi.bertsotan_android/files/huge_vocab.scorer
        //   storage/emulated/0/Android/data/eus.ikusimakusi.bertsotan_android/files/model.tflite
        try {
            Log.d(this::class.simpleName, "erauziFitxategia() - Nora: " + toFile.absolutePath)
            fromInputStream.use { fis ->
                FileOutputStream(toFile).use { os ->
                    val buffer = ByteArray(1024)
                    var len: Int
                    while (fis.read(buffer).also { len = it } != -1) {
                        os.write(buffer, 0, len)
                    }
                    os.close()
                }
                fis.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}