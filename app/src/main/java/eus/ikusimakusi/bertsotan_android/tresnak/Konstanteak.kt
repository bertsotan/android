package eus.ikusimakusi.bertsotan_android.tresnak

object Konstanteak {

    // Aplikazioa
    const val APLIKAZIO_BERTSIO_ZENBAKIA = "1.0"
    const val APLIKAZIO_ARGITARATZE_DATA = "2023/02/27"

    // Baimenak
    const val BAIMENA_RECORD_AUDIO_ESKARI_KODEA = 1

    // Ezarpenak (SharedPreferences)
    const val EZARPENAK_IZENA = "BertsotanAppPreferences"
    const val EZARPENAK_SOINUA = "soinua_data"
    const val EZARPENAK_SOINUA_LEHENETSIA = true
    const val EZARPENAK_ASR = "asr_data"
    const val EZARPENAK_ASR_LEHENETSIA = true

    // JSON errimategia
    const val JSON_FORMATUA = "1.0"

    // ASR
    const val ASR_BERTSIOA = "v1"
    const val ASR_TFLITE_IZENA = "model_$ASR_BERTSIOA.tflite"
    const val ASR_TFLITE_TAMAINA = 47_332_120
    const val ASR_SCORER_IZENA = "kenlm_$ASR_BERTSIOA.scorer"
    const val ASR_SCORER_TAMAINA = 323_250_032
    // Borobildu ( ( 47_332_120 + 323_250_032 ) / 1024 / 1024 = 363
    const val ASR_MB = 363

    // Mezuak (snackbar)
    const val MEZU_MOTA_INFORMAZIOA = "informazioa"
    const val MEZU_MOTA_ABISUA = "abisua"
    const val MEZU_MOTA_ERROREA = "errorea"
    const val MEZU_MOTA_ARRAKASTA = "arrakasta"

    // Errima familiak
    const val FAMILIA_BEGIRADA_HIZKIA = "B"
    const val FAMILIA_KOPETA_HIZKIA = "K"
    const val FAMILIA_AMONA_HIZKIA = "M"
    const val FAMILIA_SUGEA_HIZKIA = "S"

    // Bilatu errimak
    const val BILAKETA_MOTA_HIZKIKA = "hizkika"
    const val BILAKETA_MOTA_FAMILIAKA = "familiaka"
    const val ERRIMA_KOPURU_LEHENETSIA = 4
    const val BILATU_ERRIMAK_ZERRENDA_ERRIMARENAK = "errimarenak"
    const val BILATU_ERRIMAK_ZERRENDA_FAMILIAKOAK = "familiakoak"
    const val BILATU_ERRIMAK_ZERRENDA_HAUTATUAK = "hautatuak"
    const val BILATU_ERRIMAK_URL_ELHUYAR = "https://hiztegiak.elhuyar.eus/eu/"
    const val BILATU_ERRIMAK_URL_EUSKALTZAINDIA = "https://www.euskaltzaindia.eus/index.php?option=com_hiztegianbilatu&task=bilaketa&lang=eu&nondik=0&zenbat=100&non=sarreraBuruaStrict&query="

    // Honi buruz
    const val HONI_BURUZ_URL_APLIKAZIOA = "https://gitlab.com/bertsotan/android/"
    const val HONI_BURUZ_URL_ERRIMAK = "https://gitlab.com/bertsotan/errimategia"
}