package eus.ikusimakusi.bertsotan_android.models

data class Errimategia(
    val formatua: String,
    val data: String,
    val gaiak: List<String>,
    val bukaerak: List<Bukaera>,
    val hitzak: List<Hitza>
)
