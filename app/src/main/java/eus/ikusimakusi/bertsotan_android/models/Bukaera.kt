package eus.ikusimakusi.bertsotan_android.models

data class Bukaera(
    val ahoskera1: String,
    val ahoskera2: String
)
