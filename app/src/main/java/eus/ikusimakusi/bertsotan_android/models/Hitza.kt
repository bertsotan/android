package eus.ikusimakusi.bertsotan_android.models

data class Hitza(
    val hitza: String,
    val maiuskularikDu: Boolean,
    val hutsunerikDu: Boolean,
    val ahoskera1: String,
    val ahoskera2: String,
    val bukaera: String,
    val erabilgarritasuna: Int,
    var polisemikoaDa: Boolean,
    var lokalaDa: Boolean,
    var lagunartekoaDa: Boolean,
    var erderakadaDa: Boolean,
    var deklinatutaDago: Boolean,
    var iruzkina: String
)