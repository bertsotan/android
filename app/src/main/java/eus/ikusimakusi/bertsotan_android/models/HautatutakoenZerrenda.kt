package eus.ikusimakusi.bertsotan_android.models

import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak

class HautatutakoenZerrenda {
    private var leheneratuta = false
    var hautatutakoHitzak = ArrayList<HitzaZerrendan>()
    private var hutsikDaudenak = -1
    var fokuaDuena = -1

    fun kopurua(): Int{
        // Zerrendan guztira zenbat hitz dauden erantzuten du, hutsik zein beteta

        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        return this.hautatutakoHitzak.size
    }

    fun kopuruaHutsik(): Int{
        // Zerrendan hutsi zenbat hitz dauden erantzuten du

        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        return this.hutsikDaudenak
    }

    fun kopuruaBeteta(): Int{
        // Zerrendan zenbat hitz beteta dauden erantzuten du

        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        return (this.hautatutakoHitzak.size - this.hutsikDaudenak)
    }

    fun init() {
        // Objektua leheneratzen du
        // Honelako objektu bat erabili haurretik init() metodoa deitu behar da
        init(Konstanteak.ERRIMA_KOPURU_LEHENETSIA.toInt())
    }

    fun init(errimaKopurua: Int){
        // Objektua leheneratzen du
        // Honelako objektu bat erabili haurretik init() metodoa deitu behar da

        // Hautatutako hitzak prestatu
        this.hautatutakoHitzak.clear()
        this.fokuaDuena = -1

        var i = 1
        do{
            val hitzHautatuHutsa = HitzaZerrendan("")
            this.hautatutakoHitzak.add(hitzHautatuHutsa)
            i++
        } while (i <= errimaKopurua)

        this.hutsikDaudenak = errimaKopurua

        this.leheneratuta = true
    }

    fun gehitu(hitza: HitzaZerrendan, posizioa: Int){
        // Hutsik dagoen leku batean hitza ordezkatzen du
        // txertatu() metodoak zerrenda handitzen du, honek berriz ez
        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        if (!this.hautatutakoHitzak[posizioa].hutsikDago()){
            throw Exception("$posizioa posizioa ez dago hutsik!")
        }else{
            this.hautatutakoHitzak[posizioa] = hitza
            this.hutsikDaudenak--
        }
    }

    fun gehitu(hitza: HitzaZerrendan){
        // Hutsik dagoen lehen lekuan hitza ordezkatzen du
        // txertatu() metodoak zerrenda handitzen du, honek berriz ez
        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        if (this.hutsikDaudenak <= 0) {
            throw Exception("Ez dago lekurik zerrendan!")
        }

        // Hutsik dagoen lehenengo lekuan gehitu
        for (i in this.hautatutakoHitzak.indices) {
            if (this.hautatutakoHitzak[i].hutsikDago()){
                gehitu(hitza, i)
                break
            }
        }
    }

    fun txertatu (){
        // Txertatu hitz hutsa zerrendaren bukaeran
        // gehitu() metodoak hitz huts bat ordezkatzen du, honek berriz zerrenda handitzen du
        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        // Txertatu bukaeran
        val hitzHautatuHutsa = HitzaZerrendan("")
        txertatu(hitzHautatuHutsa, this.hautatutakoHitzak.size)
    }

    fun txertatu (hitza: HitzaZerrendan){
        // Txertatu hitza zerrendaren bukaeran
        // gehitu() metodoak hitz huts bat ordezkatzen du, honek berriz zerrenda handitzen du
        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        // Txertatu bukaeran
        txertatu(hitza, this.hautatutakoHitzak.size)
    }

    fun txertatu (posizioa: Int){
        // Txertatu hitz huts bat zehaztutako posizioan
        // gehitu() metodoak hitz huts bat ordezkatzen du, honek berriz zerrenda handitzen du
        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        // Txertatu bukaeran
        val hitzHautatuHutsa = HitzaZerrendan("")
        txertatu(hitzHautatuHutsa, posizioa)
    }

    fun txertatu (hitza: HitzaZerrendan, posizioa: Int){
        // Txertatu hitza zehaztutako posizioan
        // gehitu() metodoak hitz huts bat ordezkatzen du, honek berriz zerrenda handitzen du
        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        if (posizioa > this.hautatutakoHitzak.size + 1) {
            throw Exception("Posizioa okerra, zerrenda ez da hain handia!")
        }else{
            //Txertatu posizioan
            this.hautatutakoHitzak.add(posizioa, hitza)
            if (hitza.hutsikDago()){
                this.hutsikDaudenak++
            }
        }
    }

    fun kendu(posizioa: Int) {
        // Hitzaren ordez huts bat jarri
        // atera() metodoak zerrenda txikitzen du, honek berriz ez
        if (!leheneratuta) {
            throw Exception("Objektua erabili aurretik init() deitu behar duzu")
        }

        if (posizioa > this.hautatutakoHitzak.size) {
            throw Exception("Hitz hori ez da existitzen!")
        }else if (this.hautatutakoHitzak[posizioa].hutsikDago()){
            throw Exception("Hitza dagoeneko hutsik dago!")
        }else{
            this.hautatutakoHitzak[posizioa] = HitzaZerrendan("")
            this.hutsikDaudenak++
        }
    }

    fun atera(posizioa: Int){
        // Hitz hori zerrendatik atera, zerrenda bera txikituz
        // kendu() metodoak zerrendako hitz kopurua mantentzen du, honek berriz ez
        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        if (posizioa > this.hautatutakoHitzak.size) {
            throw Exception("Hitz hori ez da existitzen!")
        }else{
            this.hautatutakoHitzak.removeAt(posizioa)
        }
    }

    fun fokuaEzarri(posizioa: Int){
        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        if (posizioa > this.hautatutakoHitzak.size) {
            throw Exception("Hitz hori ez da existitzen!")
        }else{
            this.fokuaDuena = posizioa
        }
    }

    fun fokuaBadago(): Boolean{
        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        return (this.fokuaDuena != -1)
    }

    fun fokuaNon(): Int{
        if (!leheneratuta){ throw Exception("Objektua erabili aurretik init() deitu behar duzu") }

        return this.fokuaDuena
    }
}