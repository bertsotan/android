package eus.ikusimakusi.bertsotan_android.models

class HitzaZerrendan {

    // HitzaEntity-ko propietateak
    var hitza: String? = String()
    var maiuskularikDu = false
    var hutsunerikDu = false
    var ahoskera1 = String()
    var ahoskera2 = String()
    var bukaera = String()
    var erabilgarritasuna = 0
    var polisemikoaDa = false
    var lokalaDa = false
    var lagunartekoaDa = false
    var erderakadaDa = false
    var deklinatutaDago = false
    var iruzkina: String? = String()

    // Zerrendarako propietateak
    var zerrendaMota = String()
    var hitzaEskuz: String? = String()
    var eskuzkoaDa = false
    var hautatutaDago = false
    var desgaitutaDago = false
    var puntuazioa = 0
    var puntuazioarenKalkuluaAmaitutaDago = false

    constructor(
        hitza: String,
        maiuskularikDu: Boolean,
        hutsunerikDu: Boolean,
        ahoskera1: String,
        ahoskera2: String,
        bukaera: String,
        erabilgarritasuna: Int,
        polisemikoaDa: Boolean,
        lokalaDa: Boolean,
        lagunartekoaDa: Boolean,
        erderakadaDa: Boolean,
        deklibatutaDago: Boolean,
        iruzkina: String
    ){
        this.hitza = hitza
        this.maiuskularikDu = maiuskularikDu
        this.hutsunerikDu = hutsunerikDu
        this.ahoskera1 = ahoskera1
        this.ahoskera2 = ahoskera2
        this.bukaera = bukaera
        this.erabilgarritasuna = erabilgarritasuna
        this.polisemikoaDa = polisemikoaDa
        this.lokalaDa = lokalaDa
        this.lagunartekoaDa = lagunartekoaDa
        this.erderakadaDa = erderakadaDa
        this.deklinatutaDago = deklibatutaDago
        this.iruzkina = iruzkina

        this.hitzaEskuz = null
        this.eskuzkoaDa = false
    }

    constructor(
        hitzaEskuz: String,
    ){
        this.hitzaEskuz = hitzaEskuz
        this.eskuzkoaDa = true
    }

    fun kopia(): HitzaZerrendan {
        return HitzaZerrendan(
            this.hitza!!,
            maiuskularikDu,
            hutsunerikDu,
            ahoskera1,
            ahoskera2,
            bukaera,
            erabilgarritasuna,
            polisemikoaDa,
            lokalaDa,
            lagunartekoaDa,
            erderakadaDa,
            deklinatutaDago,
            iruzkina!!
        )
    }

    fun hutsikDago(): Boolean{
        if (this.eskuzkoaDa){
            return (this.hitzaEskuz == "")
        }else{
            return (this.hitza == "")
        }
    }

    fun lortuHitza(): String{
        if (this.eskuzkoaDa){
            return this.hitzaEskuz!!
        }else{
            return this.hitza!!
        }
    }
    private fun koinzidentziaHizkiak(katea1: String, katea2: String): String{
        var koinzidentzia = ""
        for (i in katea1.indices){
            if(katea2.length <= i){
                break
            }
            if(katea1[katea1.length - 1 - i] == katea2[katea2.length - 1 - i]){
                koinzidentzia += katea1[katea1.length - 1 - i]
            }else{
                break
            }
        }
        return koinzidentzia
    }

    private fun koinzidentziaHizkiKopurua(katea1: String, katea2: String): Int{
        return koinzidentziaHizkiak(katea1, katea2).length
    }

    private fun kalkulatuOinarrizkoPuntuazioa(
        bilatutakoHitzaAhoskera1: String,
        bilatutakoHitzaAhoskera2: String
    ): Int {
        var oinarrizkoPuntuazioa = 0

        // Koinzidentzia kopurua Ahoskera1en (-6)
        val koinzidentziakAhoskera1 = koinzidentziaHizkiak(bilatutakoHitzaAhoskera1, this.ahoskera1)
        oinarrizkoPuntuazioa += koinzidentziakAhoskera1.length * -6

        // Koinzidentzia kopurua Ahoskera2n (-3)
        val koinzidentziakAhoskera2 = koinzidentziaHizkiak(bilatutakoHitzaAhoskera2, this.ahoskera2)
        oinarrizkoPuntuazioa += koinzidentziakAhoskera2.length * -3

        // Soberako hizki kopurua Ahoskera1en (+2)
        val puntuazioaSoberakoHizkiak = (this.ahoskera1.length - koinzidentziakAhoskera1.length) * 2
        oinarrizkoPuntuazioa += puntuazioaSoberakoHizkiak

        return oinarrizkoPuntuazioa
    }

    fun kalkulatuHitzarenPuntuazioa(
        bilatutakoHitza: String,
        bilatutakoHitzaAhoskera1: String,
        bilatutakoHitzaAhoskera2: String
    ) {
        // TODO maiztasuna
        /*
        Errimaren zerrenda / Familiakoen zerrenda / Izenen zerrenda / Gaikoen zerrenda
        ------------------------------------------------------------------------------
        p=0
        1) Hitza Case Sensitive berdina. (p=1)
        2) Hitza Case Insensitive berdina(k). (p=2)
        3) Ahoskera1 berdina(k). (p=3)
        4) Ahoskera2 berdina(k). (p=4)
        p == 0
           Ez da automatikoa:
              5) Polisemikoa da (p=100)
                 - Koinzidentzia kopurua Ahoskera1en (-6)
                 - Koinzidentzia kopurua Ahoskera2n (-3)
                 - Soberako hizki kopurua Ahoskera1en (+2)
              Ez da polisemikoa:
                 Ez du maiuskularik:
                    6) Ez da Deklinatua (p=200)
                       - Koinzidentzia kopurua Ahoskera1en (-6)
                       - Koinzidentzia kopurua Ahoskera2n (-3)
                       - Soberako hizki kopurua Ahoskera1en (+2)
                    7) Deklinatua da (p=300)
                       - Koinzidentzia kopurua Ahoskera1en (-6)
                       - Koinzidentzia kopurua Ahoskera2n (-3)
                       - Soberako hizki kopurua Ahoskera1en (+2)
                 8) Maiuskularik du (p=400)
                    - Koinzidentzia kopurua Ahoskera1en (-6)
                    - Koinzidentzia kopurua Ahoskera2n (-3)
                    - Soberako hizki kopurua Ahoskera1en (+2)
           Automatiko da:
              Ez du maiuskularik:
                9) Automatikoa da (p=500)
                   - Koinzidentzia kopurua Ahoskera1en (-6)
                   - Koinzidentzia kopurua Ahoskera2n (-3)
                   - Soberako hizki kopurua Ahoskera1en (+2)
              10) Maiuskularik du (p=600)
                  - Koinzidentzia kopurua Ahoskera1en (-6)
                  - Koinzidentzia kopurua Ahoskera2n (-3)
                  - Soberako hizki kopurua Ahoskera1en (+2)
        */
        var puntuazioa = 0

        if (bilatutakoHitza == this.hitza){
            // 1) Hitza Case Sensitive berdina. (1)
            puntuazioa = 1
        }else if (bilatutakoHitza.lowercase() == this.hitza?.lowercase()){
            // 2) Hitza Case Insensitive berdina(k). (2)
            puntuazioa = 2
        }else if (bilatutakoHitzaAhoskera1 == this.ahoskera1){
            // 3) Ahoskera1 berdina(k). (3)
            puntuazioa = 3
        }else if (bilatutakoHitzaAhoskera2 == this.ahoskera2){
            // 4) Ahoskera2 berdina(k). (4)
            puntuazioa = 4
        }
        //Log.d(this::class.simpleName, "puntuazioa: ${puntuazioa} (${this.hitza})")

        if (puntuazioa == 0){
            val oinarrizkoPuntuazioa = kalkulatuOinarrizkoPuntuazioa(
                bilatutakoHitza,
                bilatutakoHitzaAhoskera1
            )
            //Log.d(this::class.simpleName, "oinarrizkoPuntuazioa: ${oinarrizkoPuntuazioa} (${this.hitza})")
            if (!false){ // TODO Lehen: !this.automatikoaDa
                if (this.polisemikoaDa){
                    // 5) Polisemikoa da (p=100)
                    puntuazioa = 100 + oinarrizkoPuntuazioa
                }else{
                    if (!this.maiuskularikDu){
                        if (!this.deklinatutaDago){
                            // 6) Ez da Deklinatua (p=200)
                            puntuazioa = 200 + oinarrizkoPuntuazioa
                        }else{
                            // 7) Deklinatua da (p=300)
                            puntuazioa = 300 + oinarrizkoPuntuazioa
                        }
                    }else{
                        // 8) Maiuskularik du (p=400)
                        puntuazioa = 400 + oinarrizkoPuntuazioa
                    }
                }
            }else{
                if (!this.maiuskularikDu){
                    // 9) Automatikoa da (p=500)
                    puntuazioa = 500 + oinarrizkoPuntuazioa
                }else{
                    // 10) Maiuskularik du (p=600)
                    puntuazioa = 600 + oinarrizkoPuntuazioa
                }
            }
        }
        //Log.d(this::class.simpleName, "puntuazioa: ${puntuazioa} (${this.hitza})")

        this.puntuazioa = puntuazioa
    }
}
