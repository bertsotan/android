package eus.ikusimakusi.bertsotan_android.activities

import ai.coqui.libstt.STTModel
import android.Manifest
import android.Manifest.permission.RECORD_AUDIO
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.android.material.navigation.NavigationView
import eus.ikusimakusi.bertsotan_android.ErrimategiaApp
import eus.ikusimakusi.bertsotan_android.R
import eus.ikusimakusi.bertsotan_android.adapters.HitzaAdapter
import eus.ikusimakusi.bertsotan_android.adapters.IkusiErrimakAdapter
import eus.ikusimakusi.bertsotan_android.database.BukaeraEntity
import eus.ikusimakusi.bertsotan_android.database.HitzaDao
import eus.ikusimakusi.bertsotan_android.database.HitzaEntity
import eus.ikusimakusi.bertsotan_android.databinding.ActivityBilatuErrimakBinding
import eus.ikusimakusi.bertsotan_android.databinding.DialogErrimaKopuruaBinding
import eus.ikusimakusi.bertsotan_android.databinding.DialogIkusiErrimaBinding
import eus.ikusimakusi.bertsotan_android.databinding.DialogIkusiHautatutakoErrimakBinding
import eus.ikusimakusi.bertsotan_android.models.HautatutakoenZerrenda
import eus.ikusimakusi.bertsotan_android.models.HitzaZerrendan
import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak
import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak.BAIMENA_RECORD_AUDIO_ESKARI_KODEA
import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak.BILATU_ERRIMAK_URL_ELHUYAR
import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak.BILATU_ERRIMAK_URL_EUSKALTZAINDIA
import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak.MEZU_MOTA_ARRAKASTA
import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak.MEZU_MOTA_ERROREA
import eus.ikusimakusi.bertsotan_android.tresnak.Tresnak
import kotlinx.android.synthetic.main.activity_bilatu_errimak.*
import kotlinx.android.synthetic.main.activity_ezarpenak.view.*
import kotlinx.android.synthetic.main.app_bar_bilatu_errimak.*
import kotlinx.android.synthetic.main.app_bar_ezarpenak.view.*
import kotlinx.android.synthetic.main.content_bilatu_errimak.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.lang.Integer.min

class BilatuErrimakActivity: OinarrizkoActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var layout: View

    private lateinit var binding: ActivityBilatuErrimakBinding

    private var model: STTModel? = null
    private var transcriptionThread: Thread? = null
    private var isRecording: Boolean = false

    private var errimarenHitzenZerrenda = ArrayList<HitzaZerrendan>()
    private var familiakoenHitzenZerrenda = ArrayList<HitzaZerrendan>()
    private var hautatutakoHitzenZerrenda = HautatutakoenZerrenda()

    private var bilaketaMota: String = ""
    private var bilatutakoHitzaString: String = ""
    private var bilatutakoHitza: HitzaZerrendan? = null
    private var bilatutakoAhoskera1: String = ""
    private var bilatutakoAhoskera2: String = ""

    private var ikusiHautatutakoErrimakDialogoa: Dialog? = null

    private fun checkAudioPermission() {
        // Permission is automatically granted on SDK < 23 upon installation.
        if (Build.VERSION.SDK_INT >= 23) {
            val permission = Manifest.permission.RECORD_AUDIO

            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(permission), 3)
            }
        }
    }

    private fun transcribe() {
        // We read from the recorder in chunks of 2048 shorts. With a model that expects its input
        // at 16000Hz, this corresponds to 2048/16000 = 0.128s or 128ms.
        val audioBufferSize = 2048
        val audioData = ShortArray(audioBufferSize)

        runOnUiThread {
            binding.drawerLayout.tvGrabatzen.text = ""
            binding.drawerLayout.llGrabatzen.visibility = View.VISIBLE
            binding.drawerLayout.fabGrabatu.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ikonoa_stop_24))
        }

        model?.let { model ->
            val streamContext = model.createStream()

            checkAudioPermission()

            val recorder = AudioRecord(
                MediaRecorder.AudioSource.VOICE_RECOGNITION,
                model.sampleRate(),
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                audioBufferSize
            )
            recorder.startRecording()

            while (isRecording) {
                recorder.read(audioData, 0, audioBufferSize)
                model.feedAudioContent(streamContext, audioData, audioData.size)
                val decoded = model.intermediateDecode(streamContext)
                runOnUiThread {
                    binding.drawerLayout.tvGrabatzen.text = decoded
                }
            }

            val decoded = model.finishStream(streamContext)

            runOnUiThread {
                binding.drawerLayout.tvGrabatzen.text = decoded
                binding.drawerLayout.etBilatu.setText(decoded)
                binding.drawerLayout.llGrabatzen.visibility = View.GONE
                binding.drawerLayout.fabGrabatu.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ikonoa_mic_24))
            }

            recorder.stop()
            recorder.release()
        }
    }

    private fun createModel(): Boolean {
        val modelsPath = getExternalFilesDir(null).toString()
        val tfliteModelPath = "$modelsPath/${Konstanteak.ASR_TFLITE_IZENA}"
        val scorerPath = "$modelsPath/${Konstanteak.ASR_SCORER_IZENA}"

        for (path in listOf(tfliteModelPath, scorerPath)) {
            if (!File(path).exists()) {
                Log.e(this::class.simpleName, "File does not exist: $path")
            }
        }

        model = STTModel(tfliteModelPath)
        model?.enableExternalScorer(scorerPath)

        return true
    }

    private fun startListening() {
        if (!isRecording) {
            isRecording = true
            transcriptionThread = Thread(Runnable { transcribe() }, "Transcription Thread")
            transcriptionThread?.start()
        }
        Log.i(this::class.simpleName, resources.getString(R.string.grabatzen_hasi))
    }

    private fun stopListening() {
        Log.i(this::class.simpleName, resources.getString(R.string.grabatzen_gelditu))

        isRecording = false

        lifecycleScope.launch {
            bilatuHitza(binding.drawerLayout.tvGrabatzen.text.toString())
        }
    }

    private fun grabatu() {
        if (egiaztatuBaimenak()) {
            ezkutatuTeklatua()
            if (model == null) {
                if (!createModel()) {
                    return
                }
            }

            if (isRecording) {
                stopListening()
            } else {
                startListening()
            }
        } else {
            eskatuBaimenak();
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (model != null) {
            model?.freeModel()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBilatuErrimakBinding.inflate(layoutInflater)
        setContentView(binding.root)
        layout = binding.root

        setupActionBar()

        nav_view.setNavigationItemSelectedListener(this)

        hautatutakoHitzenZerrenda.init()
        // Log.d(this::class.simpleName, "Hitz hautagarriak: ${hautatutakoHitzenZerrenda.kopurua()}")

        // Zutabeak leheneratu
        setupErrimarenakRecyclerView()
        setupFamiliakoakRecyclerView()
        setupHautatutakoakRecyclerView()

        if (Tresnak.ezarpenakAsrGaitutaDago()){
            binding.drawerLayout.fabGrabatu.visibility = View.VISIBLE
        }else{
            binding.drawerLayout.fabGrabatu.visibility = View.GONE
        }

        binding.drawerLayout.etBilatu.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                binding.drawerLayout.btnBilatu.performClick()
                return@OnKeyListener true
            }
            false
        })

        binding.drawerLayout.btnBilatu.setOnClickListener{
            lifecycleScope.launch {
                bilatuHitza(binding.drawerLayout.etBilatu.text.toString())
            }
        }

        binding.drawerLayout.fabGrabatu.setOnClickListener {
            grabatu()
        }

        binding.drawerLayout.tvErrimarenak.setOnClickListener {
            binding.drawerLayout.rvHitzakErrimarenak.layoutManager?.scrollToPosition(0)
        }

        binding.drawerLayout.tvFamiliakoak.setOnClickListener {
            binding.drawerLayout.rvHitzakFamiliakoak.layoutManager?.scrollToPosition(0)
        }

        binding.drawerLayout.tvHautatuak.setOnClickListener {
            binding.drawerLayout.rvHitzakHautatuak.layoutManager?.scrollToPosition(0)
        }

        binding.drawerLayout.btnErrimaKopurua.setOnClickListener {
            aldatuErrimaKopurua()
        }
        binding.drawerLayout.btnErrimakKopiatu.setOnClickListener{
            kopiatuErrimak()
        }
        binding.drawerLayout.btnErrimakIkusi.setOnClickListener{
            ikusiHautatutakoErrimak()
        }

        ikusiHautatutakoErrimakDialogoa = Dialog(this, R.style.ThemeOverlay_Bertsotanandroid_FullscreenContainer)
        //ikusiHautatutakoErrimakDialogoa.setCancelable(false)  // Kanpoan klik eginez gero ixterik nahi ez badugu, hau deskomentatu
    }

    private fun lortuAhoskera1(ahoskera: String): String{

        // 1) Bazterretako hutsuneak kendu
        var ahoskera1 = ahoskera.trim()

        // 2) anycase -> lowercase
        ahoskera1 = ahoskera1.lowercase()

        // 3) ll hasieran -> i
        // ^ll,  ll -> i Llorente
        if (ahoskera1.startsWith("ll", false)) {  // Testuaren hasieran
            ahoskera1 = "i" + ahoskera1.substring(2, ahoskera1.length)
        }
        ahoskera1 = ahoskera1.replace(" ll", "i")  // Hitzaren hasieran

        // 4) Tarteko hutsuneak kendu
        ahoskera1 = ahoskera1.replace(" ", "")
        
        // 5) Gainontzeko ordezkapenak
        // ll -> l Apollo
        ahoskera1 = ahoskera1.replace("ll", "l")
        // á, é, í, ó, ú -> a, e, i, o, u
        ahoskera1 = ahoskera1.replace("á", "a")
        ahoskera1 = ahoskera1.replace("é", "e")
        ahoskera1 = ahoskera1.replace("í", "i")
        ahoskera1 = ahoskera1.replace("ó", "o")
        ahoskera1 = ahoskera1.replace("ú", "u")
        // bb -> b
        ahoskera1 = ahoskera1.replace("bb", "b")
        // tt -> t
        ahoskera1 = ahoskera1.replace("tt", "t")
        // dd -> d
        ahoskera1 = ahoskera1.replace("dd", "d")
        // ss -> s
        ahoskera1 = ahoskera1.replace("ss", "s")
        // gg -> g
        ahoskera1 = ahoskera1.replace("gg", "g")
        // mm -> m
        ahoskera1 = ahoskera1.replace("mm", "m")
        // nn -> n
        ahoskera1 = ahoskera1.replace("nn", "n")
        // ñ -> in Ikurriña, Egaña
        ahoskera1 = ahoskera1.replace("ñ", "in")  // TODO Ega\u00f1a
        // ck -> k
        ahoskera1 = ahoskera1.replace("ck", "k")
        // ph -> f
        ahoskera1 = ahoskera1.replace("ph", "f")
        // qui -> ki
        ahoskera1 = ahoskera1.replace("qui", "ki")
        // que -> ke
        ahoskera1 = ahoskera1.replace("que", "ke")
        // q -> k
        ahoskera1 = ahoskera1.replace("q", "k")
        // tch -> tx Tatcher
        ahoskera1 = ahoskera1.replace("tch", "tx")
        // ch -> tx Pinochet
        ahoskera1 = ahoskera1.replace("ch", "tx")
        // c -> k
        ahoskera1 = ahoskera1.replace("c", "k")
        // v -> b
        ahoskera1 = ahoskera1.replace("v", "b")
        // w -> u Twit, Willow, Wifi
        ahoskera1 = ahoskera1.replace("w", "u")
        // ii -> i
        ahoskera1 = ahoskera1.replace("ii", "i")
        // y -> i Yeste, Trinity
        ahoskera1 = ahoskera1.replace("y", "i")
        // ' ->
        ahoskera1 = ahoskera1.replace("'", "")
        // h ->
        ahoskera1 = ahoskera1.replace("h", "")
        // Bokal berdina bikoiztuta badago, bakarra utzi.
        // Adibideak: mahai, ama-alaba, Santa Ageda
        ahoskera1 = ahoskera1.replace("aa", "a")
        ahoskera1 = ahoskera1.replace("ee", "e")  // green!
        ahoskera1 = ahoskera1.replace("ii", "i")
        ahoskera1 = ahoskera1.replace("oo", "o")  // moon!
        ahoskera1 = ahoskera1.replace("uu", "u")

        // th -> θ ????????????? Elisabeth
        // gui -> gi Ezin da automatikoki ordezkatu! (pinguino)
        // gue -> ge Ezin da automatikoki ordezkatu! (eguerdi)
        // ing -> in Ezin da automatikoki ordezkatu! (ingelesa)
        // ee -> i Ezin da automatikoki ordezkatu! (zitekeen)
        // oo -> u Ezin da automatikoki ordezkatu! (kooperazioa, zoo, urkook)
        // TODO

        // 6) Gidoiak kendu
        ahoskera1 = ahoskera1.replace("-", "")

        return ahoskera1
    }

    private fun regexpReplaceT(kateOriginala: String): String{
        val patroiaHizkia = "t([^zsx])"
        val regex = patroiaHizkia.toRegex()
        var azpiKatea = kateOriginala
        var kateBerria = ""
        var nondikAurreraPosizioa = 0
        var nonPosizioa: Int?
        var jarraitu = true
        do {
            azpiKatea = azpiKatea.substring(nondikAurreraPosizioa, azpiKatea.length)
            nonPosizioa = regex.find(azpiKatea)?.range?.first
            if(nonPosizioa != null){
                kateBerria = kateBerria + azpiKatea.substring(0, nonPosizioa) + Konstanteak.FAMILIA_KOPETA_HIZKIA
                if (nonPosizioa < azpiKatea.length) {
                    nondikAurreraPosizioa = nonPosizioa + 1
                }else if (nonPosizioa == azpiKatea.length) {
                    jarraitu = false
                }
            }else{
                kateBerria += azpiKatea.substring(0, azpiKatea.length)
                jarraitu = false
            }
        } while (jarraitu)
        return kateBerria
    }

    private fun regexpReplaceR(kateOriginala: String): String {
        //Log.d(this::class.simpleName, "regexpReplaceR() - LEHEN: ${kateOriginala}")

        val patroiaHizkia = "[aeiou]"
        val regex = patroiaHizkia.toRegex()
        var azpiKatea = kateOriginala
        var kateBerria = ""
        var nondikAurreraPosizioa = 0
        var nonPosizioa: Int?
        var aurrekoHizkia: String
        var ondokoHizkia: String

        do {
            azpiKatea = azpiKatea.substring(nondikAurreraPosizioa, azpiKatea.length)
            nonPosizioa = azpiKatea.indexOf("r", 0, false)
            if (nonPosizioa != -1){
                if (nonPosizioa == 0) {
                    // r elehenengo hizkia da
                    aurrekoHizkia = ""
                }else{
                    aurrekoHizkia = azpiKatea.substring(nonPosizioa - 1, nonPosizioa)
                }

                if (nonPosizioa == azpiKatea.length - 1) {
                    // r azken hizkia da
                    ondokoHizkia = ""
                }else{
                    ondokoHizkia = azpiKatea.substring(nonPosizioa + 1, nonPosizioa + 2)
                }

                if (regex.find(aurrekoHizkia)?.range?.first != null && regex.find(ondokoHizkia)?.range?.first != null){
                    // r hizkiak bokal bat du aurretik eta ondoren
                    kateBerria += azpiKatea.substring(0, nonPosizioa) + Konstanteak.FAMILIA_BEGIRADA_HIZKIA
                }else{
                    kateBerria += azpiKatea.substring(0, nonPosizioa) + "r"
                }
                if (nonPosizioa < azpiKatea.length) {
                    nondikAurreraPosizioa = nonPosizioa + 1
                }
            }
        } while (nonPosizioa != -1)
        kateBerria += azpiKatea

        //Log.d(this::class.simpleName, "regexpReplaceR() - ORAIN: ${kateBerria}")
        return kateBerria
    }

    private fun lortuAhoskera2(ahoskera: String): String{
        //Log.d(this::class.simpleName,"lortuAhoskera2 - ahoskera: $ahoskera")

        // 1) Ahoskera1 lortu: minuskulaz, trinkotuta, ordezkapen fonetikoekin
        var ahoskera2 = lortuAhoskera1(ahoskera)

        // 2) begirada: b, g, d, r -> B
        ahoskera2 = ahoskera2.replace("b", Konstanteak.FAMILIA_BEGIRADA_HIZKIA)
        ahoskera2 = ahoskera2.replace("g", Konstanteak.FAMILIA_BEGIRADA_HIZKIA)
        ahoskera2 = ahoskera2.replace("d", Konstanteak.FAMILIA_BEGIRADA_HIZKIA)
        // r: Aurretik edo ondoren bokala ez den zerbait badator ez ordezkatu
        // rukula -> Bukula (u bokala da)
        // zura -> zuBa     (u eta a bokalak dira)
        // grina -> =       (g ez da bokala)
        // artoa -> =       (t ez da bokala)
        // arroa -> =       (r ez da bokala)
        // negar -> negaB   (a bokala da)
        if (ahoskera2.startsWith("r", false)) {  // Testuaren hasieran
            ahoskera2 = Konstanteak.FAMILIA_BEGIRADA_HIZKIA + ahoskera2.substring(1, ahoskera2.length)
        }
        ahoskera2 = regexpReplaceR(ahoskera2)  // Tartean
        if (ahoskera2.endsWith("r", false)) {  // Testuaren amaieran
            ahoskera2 = ahoskera2.substring(0, ahoskera2.length - 1) + Konstanteak.FAMILIA_BEGIRADA_HIZKIA
        }

        // 3) kopeta: k, p, t -> K
        ahoskera2 = ahoskera2.replace("k", Konstanteak.FAMILIA_KOPETA_HIZKIA)
        ahoskera2 = ahoskera2.replace("p", Konstanteak.FAMILIA_KOPETA_HIZKIA)
        // t: Ondoren z, s edo x badator ez ordezkatu
        // tipi tapa -> Kipi Kapa
        // fagot -> fagoK
        // txori -> =
        // etxea -> =
        // urrats -> =
        ahoskera2 = regexpReplaceT(ahoskera2)  // Tartean
        if (ahoskera2.endsWith("t", false)) {  // Testuaren amaieran
            ahoskera2 = ahoskera2.substring(0, ahoskera2.length - 1) + Konstanteak.FAMILIA_KOPETA_HIZKIA
        }

        // 4) amona: m, n -> M
        ahoskera2 = ahoskera2.replace("m", Konstanteak.FAMILIA_AMONA_HIZKIA)
        ahoskera2 = ahoskera2.replace("n", Konstanteak.FAMILIA_AMONA_HIZKIA)

        // 5) sugea: z, s, x, tz, ts, tx -> S
        ahoskera2 = ahoskera2.replace("tz", Konstanteak.FAMILIA_SUGEA_HIZKIA)
        ahoskera2 = ahoskera2.replace("ts", Konstanteak.FAMILIA_SUGEA_HIZKIA)
        ahoskera2 = ahoskera2.replace("tx", Konstanteak.FAMILIA_SUGEA_HIZKIA)
        ahoskera2 = ahoskera2.replace("z", Konstanteak.FAMILIA_SUGEA_HIZKIA)
        ahoskera2 = ahoskera2.replace("s", Konstanteak.FAMILIA_SUGEA_HIZKIA)
        ahoskera2 = ahoskera2.replace("x", Konstanteak.FAMILIA_SUGEA_HIZKIA)

        //Log.d(this::class.simpleName,"lortuAhoskera2 - return ahoskera2: $ahoskera2")
        return ahoskera2
    }

    private suspend fun bilatuHitza(bilatzekoHitzaString: String){
        withContext(Dispatchers.IO) {
            runOnUiThread {
                binding.drawerLayout.btnBilatu.isClickable = false
            }
            bistaratuAurrerabideDialogoa(resources.getString(R.string.dialogoa_itxaron))
            if (bilatzekoHitzaString.trim().isEmpty()) {
                ezkutatuAurrerabideDialogoa()
                bistaratuMezua(
                    getString(R.string.bilatu_errimak_mezua_errima_luzeagoa),
                    Konstanteak.MEZU_MOTA_ABISUA
                )
                runOnUiThread {
                    binding.drawerLayout.btnBilatu.isClickable = true
                }
            }else {

                // Bukaeraren ahoskera1 lortu
                val bilatutakoAhoskera1behinbehinean = lortuAhoskera1(bilatzekoHitzaString.trim())
                if (!bilatutakoAhoskera1behinbehinean.isNullOrEmpty() && bilatutakoAhoskera1behinbehinean.length < 3) {
                    ezkutatuAurrerabideDialogoa()
                    bistaratuMezua(
                        getString(R.string.bilatu_errimak_mezua_errima_luzeagoa),
                        Konstanteak.MEZU_MOTA_ABISUA
                    )
                    runOnUiThread {
                        binding.drawerLayout.btnBilatu.isClickable = true
                    }
                } else {

                    // Ez dugu Toast-ik bistaratuko, beraz, hau da bilatuko dugun hitza
                    bilatutakoHitzaString = bilatzekoHitzaString
                    bilaketaMota = Konstanteak.BILAKETA_MOTA_FAMILIAKA  // TODO etorkizunean aldakorra
                    bilatutakoAhoskera1 = bilatutakoAhoskera1behinbehinean
                    bilatutakoAhoskera2 = lortuAhoskera2(bilatutakoAhoskera1)
                    // Errimaren hitzen zutabea hutsitu
                    errimarenHitzenZerrenda?.clear()
                    runOnUiThread {
                        //binding.drawerLayout.rvHitzakErrimarenak.layoutManager = null
                        //Log.d(this::class.simpleName, "rvHitzakErrimarenak null")
                        //binding.drawerLayout.rvHitzakErrimarenak.adapter = null
                        binding.drawerLayout.rvHitzakErrimarenak.removeAllViewsInLayout()
                        setupErrimarenakRecyclerView()
                    }
                    // Familakoen hitzen zutabea hutsitu
                    familiakoenHitzenZerrenda?.clear()
                    runOnUiThread {
                        //binding.drawerLayout.rvHitzakFamiliakoak.layoutManager = null
                        //Log.d(this::class.simpleName, "rvHitzakFamiliakoak null")
                        //binding.drawerLayout.rvHitzakFamiliakoak.adapter = null
                        binding.drawerLayout.rvHitzakFamiliakoak.removeAllViewsInLayout()
                        setupFamiliakoakRecyclerView()
                    }
                    // Hautatutako hitzen zutabea hutsitu
                    Log.d(this::class.simpleName, "hautatutakoHitzenZerrenda.init - kopurua: ${hautatutakoHitzenZerrenda.kopurua()}")
                    hautatutakoHitzenZerrenda.init(hautatutakoHitzenZerrenda.kopurua())
                    runOnUiThread {
                        binding.drawerLayout.rvHitzakHautatuak.removeAllViewsInLayout()
                        setupHautatutakoakRecyclerView()
                        // Ezkutatu teklatua bilaketak dirauen artean
                        ezkutatuTeklatua()
                    }

                    // Kalkulatu hitzaren bukaera
                    val bukaera1String = bilatutakoAhoskera1.substring(bilatutakoAhoskera1.length - 3)
                    var bukaera2String: String
                    val familiakoBukaerenZerrendaString = ArrayList<String>()
                    Log.d(this::class.simpleName,"Bilatutakoa hitza (literala): $bilatutakoHitzaString")
                    Log.d(this::class.simpleName,"Bilatutakoa hitzaren ahoskera (ahoskera1 / ahoskera2): $bilatutakoAhoskera1 / $bilatutakoAhoskera2")
                    Log.d(this::class.simpleName,"Bilatutakoa hitzaren ahoskeraren bukaera (ahoskera1): $bukaera1String")

                    lifecycleScope.launch {

                        // Errimaren bukaerak lortu
                        val bukaeraDao = (application as ErrimategiaApp).db.bukaeraDao()

                        bukaeraDao.fetchBukaerakByAhoskera1(bukaera1String).collect {
                            val errimarenBukaerenZerrendaEntity: ArrayList<BukaeraEntity> =
                                ArrayList(it)
                            Log.d(
                                this@BilatuErrimakActivity::class.simpleName,
                                "errimarenBukaerenZerrendaEntity.size: ${errimarenBukaerenZerrendaEntity.size}"
                            )
                            val errimarekinBadagoBukaera: Boolean
                            if (errimarenBukaerenZerrendaEntity.isEmpty() || errimarenBukaerenZerrendaEntity.size != 1) {
                                errimarekinBadagoBukaera = false
                                // Errimarekin ez dago bukaerarik, familiako bukaera gero eskuz prestatu beharko da
                                bukaera2String = lortuAhoskera2(bilatutakoHitzaString)
                                bukaera2String = bukaera2String.substring(bukaera2String.length - 3)
                            } else {
                                errimarekinBadagoBukaera = true
                                // Errimaren bukaera daukagu, lortu familiako bukaera
                                bukaera2String = errimarenBukaerenZerrendaEntity[0].ahoskera2
                            }
                            Log.d(
                                this::class.simpleName,
                                "Bilatutakoa hitzaren ahoskeraren bukaera (ahoskera1 / ahoskera2): $bukaera1String / $bukaera2String"
                            )

                            bukaeraDao.fetchBukaerakByAhoskera2EzAhoskera1(
                                bukaera2String,
                                bukaera1String
                            ).collect {
                                val familiakoBukaerenZerrendaEntity: ArrayList<BukaeraEntity> =
                                    ArrayList(it)
                                Log.d(
                                    this@BilatuErrimakActivity::class.simpleName,
                                    "familiakoBukaerenZerrendaEntity.size: ${familiakoBukaerenZerrendaEntity.size}"
                                )
                                for (bukaeraEntity in familiakoBukaerenZerrendaEntity) {
                                    familiakoBukaerenZerrendaString.add(bukaeraEntity.ahoskera1)
                                }
                                if (!errimarekinBadagoBukaera) {
                                    familiakoBukaerenZerrendaString.add(bukaera1String) // TODO bukaera2String ??
                                    Log.d(
                                        this@BilatuErrimakActivity::class.simpleName,
                                        "Errimarekin ez dago bukaerarik, prestatu familiako bukaera (I)"
                                    )
                                }
                                for (bukaera in familiakoBukaerenZerrendaString) {  // TODO logak kendu
                                    Log.d(
                                        this@BilatuErrimakActivity::class.simpleName,
                                        "Bilatutako hitzaren familiako bukaerak zerrendatzen: $bukaera"
                                    )
                                }

                                bistaratuErrimarenakFamiliakoak(
                                    bukaera1String,
                                    familiakoBukaerenZerrendaString
                                )

                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun bistaratuErrimarenakFamiliakoak(bukaera1String: String, familiakoBukaerenZerrendaString: ArrayList<String>) {
        withContext(Dispatchers.IO) {
            Log.d(
                this@BilatuErrimakActivity::class.simpleName,
                "bistaratuErrimarenakFamiliakoak() HASI"
            )
            val hitzaDao = (application as ErrimategiaApp).db.hitzaDao()

            // Errimaren hitzak lortu
            CoroutineScope(Dispatchers.IO).launch {
                hitzaDao.fetchHitzakByBukaera(bukaera1String)
                    .collect {
                        val errimarenZerrendaEntity: ArrayList<HitzaEntity> = ArrayList(it)
                        var caseSensitive: HitzaZerrendan? = null
                        var caseInsensitive: HitzaZerrendan? = null
                        Log.d(
                            this@BilatuErrimakActivity::class.simpleName,
                            "'$bukaera1String' bukaera duten hitz kopurua: ${errimarenZerrendaEntity.size}"
                        )
                        for (hitzaEntity in errimarenZerrendaEntity) {
                            // Hitz hori bere horretan idatzita eta case insensitive zerrendan dagoen begiratu
                            val hitzaZerrendan = HitzaZerrendan(
                                hitzaEntity.hitza,
                                hitzaEntity.maiuskularikDu,
                                hitzaEntity.hutsunerikDu,
                                hitzaEntity.ahoskera1,
                                hitzaEntity.ahoskera2,
                                hitzaEntity.bukaera,
                                hitzaEntity.erabilgarritasuna,
                                hitzaEntity.polisemikoaDa,
                                hitzaEntity.lokalaDa,
                                hitzaEntity.lagunartekoaDa,
                                hitzaEntity.erderakadaDa,
                                hitzaEntity.deklinatutaDago,
                                hitzaEntity.iruzkina
                            )
                            if (hitzaZerrendan.hitza == bilatutakoHitzaString) {
                                caseSensitive = hitzaZerrendan
                            }
                            if (hitzaZerrendan.hitza?.lowercase() == bilatutakoHitzaString.lowercase()) {
                                caseInsensitive = hitzaZerrendan
                            }
                            errimarenHitzenZerrenda.add(hitzaZerrendan)
                        }

                    if (caseSensitive != null && caseSensitive.hitza?.isNotEmpty() == true) {
                        // Bere horretan zerrendan badago, hori hautatu eta zerrendatik kendu
                        bilatutakoHitzaString = caseSensitive.hitza!!
                        bilatutakoHitza = caseSensitive
                    } else {
                        if (caseInsensitive != null && caseSensitive?.hitza?.isNotEmpty() == true) {
                            // Bestela, hitza case insensitive aurkitu den azkena hautatu eta zerrendatik kendu
                            bilatutakoHitzaString = caseSensitive.hitza!!
                            bilatutakoHitza = caseInsensitive
                        }
                    }

                    // Bilatutako hitza berrezarri, badaezpada maiuskulak/minuskulak aldatu nahi ditugun
                    runOnUiThread {
                        binding.drawerLayout.etBilatu.setText(bilatutakoHitzaString)
                    }

                    // Hitz hori aurkitu bada, zerrendan desgaitu polisemikoa ez bada eta hautatutakoen azkenera gehitu
                    for (hitzaZerrendan in errimarenHitzenZerrenda) {
                        if (hitzaZerrendan.hitza == bilatutakoHitzaString && !hitzaZerrendan.polisemikoaDa) {
                            hitzaZerrendan.desgaitutaDago = true
                            Log.d(
                                this@BilatuErrimakActivity::class.simpleName,
                                "Bilatutako hitza zerrendan desgaitu: $bilatutakoHitzaString"
                            )
                        }
                    }

                    // Errimaren hitzen zutabea eta Hautatutakoena bete
                    setupErrimarenakRecyclerView()
                    setupHautatutakoakRecyclerView()

                    // Familiakoen hitzen zutabean zer bistaratu argitu
                    bistaratuFamiliakoak(familiakoBukaerenZerrendaString, hitzaDao)
                }
            }
        }
    }

    private suspend fun bistaratuFamiliakoak(familiakoBukaerenZerrendaString: ArrayList<String>, hitzaDao: HitzaDao) {
        withContext(Dispatchers.IO) {

            // Familiako hitzak lortu
            CoroutineScope(Dispatchers.IO).launch {
                hitzaDao.fetchHitzakByBukaerak(familiakoBukaerenZerrendaString).collect {
                    val familiarenZerrendaEntity: ArrayList<HitzaEntity> =
                        ArrayList(it)
                    Log.d(
                        this@BilatuErrimakActivity::class.simpleName,
                        "'$familiakoBukaerenZerrendaString' bukaerak dituzten hitz kopurua: ${familiarenZerrendaEntity.size}"
                    )

                    for (hitzaEntity in familiarenZerrendaEntity) {
                        // Hitz hori bere horretan idatzita eta case insensitive zerrendan dagoen begiratu
                        val hitzaZerrendan = HitzaZerrendan(
                            hitzaEntity.hitza,
                            hitzaEntity.maiuskularikDu,
                            hitzaEntity.hutsunerikDu,
                            hitzaEntity.ahoskera1,
                            hitzaEntity.ahoskera2,
                            hitzaEntity.bukaera,
                            hitzaEntity.erabilgarritasuna,
                            hitzaEntity.polisemikoaDa,
                            hitzaEntity.lokalaDa,
                            hitzaEntity.lagunartekoaDa,
                            hitzaEntity.erderakadaDa,
                            hitzaEntity.deklinatutaDago,
                            hitzaEntity.iruzkina
                        )
                        familiakoenHitzenZerrenda.add(hitzaZerrendan)
                    }

                    // Familiako hitzen zutabea bete
                    setupFamiliakoakRecyclerView()

                    // Bilaketa amaitu da eta 3 zutabeak eguneratu dira, Itxaron dialogoa itxi
                    ezkutatuAurrerabideDialogoa()

                    runOnUiThread {
                        binding.drawerLayout.btnBilatu.isClickable = true
                    }
                }
            }
        }
    }

    private fun setupActionBar(){
        setSupportActionBar(toolbar_bilatu_errimak)
        toolbar_bilatu_errimak.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
        toolbar_bilatu_errimak.setNavigationIcon(R.drawable.ikonoa_burgerra_24)

        toolbar_bilatu_errimak.setNavigationOnClickListener {
            toggleDrawer()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            toggleDrawer()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun toggleDrawer(){
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }else{
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }else{
            birritanAtzeraIrtetzeko()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.aukera_bilatu_errimak -> {
                // Ez egin ezer, dagoeneko bertan gaudelako
            }
            R.id.aukera_ezarpenak -> {
                val intent = Intent(this, EzarpenakActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
            R.id.aukera_irten -> {
                finishAffinity()
            }
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)

        return true
    }

    private fun hautatutakoHitzenZerrendaLeheneratuAzkenarekin(kopurua: Int){
        Log.d(this::class.simpleName, "hautatutakoHitzenZerrendaLeheneratuAzkenarekin() - kopurua: $kopurua")

        // Errimen lehenengoa hautatu (aurkitu bada), eta hautatutakoen azkenera gehitu
        hautatutakoHitzenZerrenda.init(kopurua)
        if (bilatutakoHitzaString != ""){
            if (errimarenHitzenZerrenda[0].hitza == bilatutakoHitzaString){
                errimarenHitzenZerrenda[0].desgaitutaDago = true
                errimarenHitzenZerrenda[0].zerrendaMota = Konstanteak.BILATU_ERRIMAK_ZERRENDA_ERRIMARENAK
                hautatutakoHitzenZerrenda.gehitu(errimarenHitzenZerrenda[0], hautatutakoHitzenZerrenda.kopurua() - 1)
            }else{
                val hitzaEskuz = HitzaZerrendan(bilatutakoHitzaString)
                hautatutakoHitzenZerrenda.gehitu(hitzaEskuz, hautatutakoHitzenZerrenda.kopurua() - 1)
            }
        }
    }
    private fun setupErrimarenakRecyclerView(){
        Log.d(this::class.simpleName, "setupErrimarenakRecyclerView()")

        runOnUiThread {
            if(errimarenHitzenZerrenda != null && errimarenHitzenZerrenda.isNotEmpty()){
                // Zerrenda prestatu
                for (hitza in errimarenHitzenZerrenda){
                    hitza.kalkulatuHitzarenPuntuazioa(
                        bilatutakoHitzaString,
                        bilatutakoAhoskera1,
                        bilatutakoAhoskera2
                    )
                }
                //TODO ConcurrentModificationException saihestu ArrayList bat eguneratzean
                /*var errimarenHitzenZerrendaPuntuazioarekin = ArrayList<HitzaZerrendan>()
                for (hitza in errimarenHitzenZerrenda){
                    var hitzaPuntuazioarekin = hitza.kopia()
                    hitzaPuntuazioarekin.kalkulatuHitzarenPuntuazioa(
                        bilatutakoHitzaString,
                        bilatutakoAhoskera1,
                        bilatutakoAhoskera2
                    )
                    errimarenHitzenZerrendaPuntuazioarekin.add(hitzaPuntuazioarekin)
                }
                errimarenHitzenZerrenda = errimarenHitzenZerrendaPuntuazioarekin*/

                errimarenHitzenZerrenda.sortBy { it.puntuazioa }

                // Errimen lehenengoa hautatu (aurkitu bada), eta hautatutakoen azkenera gehitu
                hautatutakoHitzenZerrendaLeheneratuAzkenarekin(hautatutakoHitzenZerrenda.kopurua())

                binding.drawerLayout.svErrimarenak.visibility = View.VISIBLE
                binding.drawerLayout.llErrimarenakEz.visibility = View.GONE

                val hitzaAdapter = HitzaAdapter(
                    Konstanteak.BILATU_ERRIMAK_ZERRENDA_ERRIMARENAK,
                    errimarenHitzenZerrenda,
                    { posizioa, hitza ->
                        aukeratuListener(
                            Konstanteak.BILATU_ERRIMAK_ZERRENDA_ERRIMARENAK,
                            errimarenHitzenZerrenda,
                            posizioa,
                            hitza
                        )
                    },
                    { posizioa, hitza ->
                        ikusiErrimaListener(
                            Konstanteak.BILATU_ERRIMAK_ZERRENDA_ERRIMARENAK,
                            errimarenHitzenZerrenda,
                            posizioa,
                            hitza
                        )
                    }
                )
                binding.drawerLayout.rvHitzakErrimarenak.layoutManager = LinearLayoutManager(this)
                Log.d(this::class.simpleName, "setupErrimarenakRecyclerView() - rvHitzakErrimarenak hitzaAdapter")
                binding.drawerLayout.rvHitzakErrimarenak.adapter = hitzaAdapter
            }else{
                binding.drawerLayout.svErrimarenak.visibility = View.GONE
                binding.drawerLayout.llErrimarenakEz.visibility = View.VISIBLE

                binding.drawerLayout.rvHitzakErrimarenak.layoutManager = null
                Log.d(this::class.simpleName, "setupErrimarenakRecyclerView() - rvHitzakErrimarenak null")
                binding.drawerLayout.rvHitzakErrimarenak.adapter = null
            }
        }
    }

    private fun setupFamiliakoakRecyclerView(){
        Log.d(this::class.simpleName, "setupFamiliakoakRecyclerView()")

        runOnUiThread {
            if(familiakoenHitzenZerrenda != null && familiakoenHitzenZerrenda.isNotEmpty()){
                // Zerrenda prestatzen
                for (hitza in familiakoenHitzenZerrenda){
                    hitza.kalkulatuHitzarenPuntuazioa(
                        bilatutakoHitzaString,
                        bilatutakoAhoskera1,
                        bilatutakoAhoskera2
                    )
                }
                //TODO ConcurrentModificationException saihestu ArrayList bat eguneratzean
                /*var familiakoenHitzenZerrendaPuntuazioarekin = ArrayList<HitzaZerrendan>()
                for (hitza in familiakoenHitzenZerrenda){
                    var hitzaPuntuazioarekin = hitza.kopia()
                    hitzaPuntuazioarekin.kalkulatuHitzarenPuntuazioa(
                        bilatutakoHitzaString,
                        bilatutakoAhoskera1,
                        bilatutakoAhoskera2
                    )
                    familiakoenHitzenZerrendaPuntuazioarekin.add(hitzaPuntuazioarekin)
                }
                familiakoenHitzenZerrenda = familiakoenHitzenZerrendaPuntuazioarekin*/

                familiakoenHitzenZerrenda.sortBy { it.puntuazioa }

                binding.drawerLayout.svFamiliakoak.visibility = View.VISIBLE
                binding.drawerLayout.llFamiliakoakEz.visibility = View.GONE

                val hitzaAdapter = HitzaAdapter(
                    Konstanteak.BILATU_ERRIMAK_ZERRENDA_FAMILIAKOAK,
                    familiakoenHitzenZerrenda,
                    { posizioa, hitza ->
                        aukeratuListener(
                            Konstanteak.BILATU_ERRIMAK_ZERRENDA_FAMILIAKOAK,
                            familiakoenHitzenZerrenda,
                            posizioa,
                            hitza
                        )
                    },
                    { posizioa, hitza ->
                        ikusiErrimaListener(
                            Konstanteak.BILATU_ERRIMAK_ZERRENDA_FAMILIAKOAK,
                            familiakoenHitzenZerrenda,
                            posizioa,
                            hitza
                        )
                    }
                )
                binding.drawerLayout.rvHitzakFamiliakoak.layoutManager = LinearLayoutManager(this)
                Log.d(this::class.simpleName, "setupFamiliakoakRecyclerView() - rvHitzakFamiliakoak hitzaAdapter")
                binding.drawerLayout.rvHitzakFamiliakoak.adapter = hitzaAdapter
            }else{
                binding.drawerLayout.svFamiliakoak.visibility = View.GONE
                binding.drawerLayout.llFamiliakoakEz.visibility = View.VISIBLE

                binding.drawerLayout.rvHitzakFamiliakoak.layoutManager = null
                Log.d(this::class.simpleName, "setupFamiliakoakRecyclerView() - rvHitzakFamiliakoak null")
                binding.drawerLayout.rvHitzakFamiliakoak.adapter = null
            }
        }
    }

    private fun setupHautatutakoakRecyclerView(){
        Log.d(this::class.simpleName, "setupHautatutakoakRecyclerView()")

        runOnUiThread {
            binding.drawerLayout.btnErrimaKopurua.text =
                String.format(
                    getString(R.string.bilatu_errimak_puntu_kopurua),
                    hautatutakoHitzenZerrenda.kopurua()
                )

            // Egiaztatu zenbat hautatu (edo eskuz idatzi diren)
            var hautatutakorikDago = false
            for (hitza in hautatutakoHitzenZerrenda.hautatutakoHitzak){
                if (hitza.hitza != "" && hitza.hitzaEskuz != ""){
                    hautatutakorikDago = true
                    break
                }
            }

            if (hautatutakorikDago) {
                binding.drawerLayout.btnErrimakKopiatu.isEnabled = true
                binding.drawerLayout.btnErrimakKopiatu.setCardBackgroundColor(ContextCompat.getColor(this.baseContext, R.color.colorPrimary))
                binding.drawerLayout.ivErrimakKopiatu.imageTintList = AppCompatResources.getColorStateList(this.baseContext, R.color.colorOnPrimary)

                binding.drawerLayout.btnErrimakIkusi.isEnabled = true
                binding.drawerLayout.btnErrimakIkusi.setCardBackgroundColor(ContextCompat.getColor(this.baseContext, R.color.colorPrimary))
                binding.drawerLayout.ivErrimakIkusi.imageTintList = AppCompatResources.getColorStateList(this.baseContext, R.color.colorOnPrimary)
            }else{
                binding.drawerLayout.btnErrimakKopiatu.isEnabled = false
                binding.drawerLayout.btnErrimakKopiatu.setCardBackgroundColor(ContextCompat.getColor(this.baseContext, R.color.colorPrimaryDisabled))
                binding.drawerLayout.ivErrimakKopiatu.imageTintList = AppCompatResources.getColorStateList(this.baseContext, R.color.colorOnPrimaryDisabled)

                binding.drawerLayout.btnErrimakIkusi.isEnabled = false
                binding.drawerLayout.btnErrimakIkusi.setCardBackgroundColor(ContextCompat.getColor(this.baseContext, R.color.colorPrimaryDisabled))
                binding.drawerLayout.ivErrimakIkusi.imageTintList = AppCompatResources.getColorStateList(this.baseContext, R.color.colorOnPrimaryDisabled)
            }

            var hitzaAdapter = HitzaAdapter(
                Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK,
                hautatutakoHitzenZerrenda.hautatutakoHitzak,
                { posizioa, hitza ->
                    aukeratuListener(
                        Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK,
                        null,  // Hautatutakoak direnean ez da behar zerrenda pasatzerik klasean bertan dagoelako
                        posizioa,
                        hitza
                    )
                },
                { posizioa, hitza ->
                    ikusiErrimaListener(
                        Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK,
                        null,  // Hautatutakoak direnean ez da behar zerrenda pasatzerik klasean bertan dagoelako
                        posizioa,
                        hitza
                    )
                }
            )

            binding.drawerLayout.rvHitzakHautatuak.layoutManager = LinearLayoutManager(this)
            Log.d(this::class.simpleName, "setupHautatutakoakRecyclerView() - rvHitzakHautatuak hitzaAdapter")
            binding.drawerLayout.rvHitzakHautatuak.adapter = hitzaAdapter

            // Arrastatu eta jaregin
            val helper = ItemTouchHelper(
                object: ItemTouchHelper.SimpleCallback(
                    ItemTouchHelper.UP or ItemTouchHelper.DOWN, 0
                ){
                    var dragFrom = -1
                    var dragTo = -1

                    override fun getMovementFlags(
                        recyclerView: RecyclerView,
                        viewHolder: RecyclerView.ViewHolder
                    ): Int {
                        return makeMovementFlags(ItemTouchHelper.UP or
                                ItemTouchHelper.DOWN,
                            0)
                    }
                    override fun onMove(
                        recyclerView: RecyclerView,
                        viewHolder: RecyclerView.ViewHolder,
                        target: RecyclerView.ViewHolder
                    ): Boolean {
                        val fromPosition = viewHolder.adapterPosition
                        val toPosition = target.adapterPosition

                        if (dragFrom == -1) {
                            dragFrom = fromPosition
                        }
                        dragTo = toPosition

                        hitzaAdapter.onItemMove(fromPosition, toPosition)

                        return true
                    }

                    private fun reallyMoved(from: Int, to: Int) {
                        Log.d("ItemTouchHelper", "setupHautatutakoakRecyclerView() - reallyMoved(): $from --> $to")

                        joSoinua(this@BilatuErrimakActivity.baseContext, R.raw.soinua_blip)

                        var hitzenLoga = "Zerrenda: "
                        for (i in hautatutakoHitzenZerrenda.hautatutakoHitzak.indices){
                            if(hautatutakoHitzenZerrenda.hautatutakoHitzak[i].lortuHitza() == ""){
                                hitzenLoga += i.toString() + ", "
                            }else{
                                hitzenLoga += hautatutakoHitzenZerrenda.hautatutakoHitzak[i].lortuHitza() + ", "
                            }
                        }
                        Log.d("BilatuErrimakActivity", "reallyMoved() - $hitzenLoga")

                        hitzaAdapter = HitzaAdapter(
                            Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK,
                            hautatutakoHitzenZerrenda.hautatutakoHitzak,
                            { posizioa, hitza ->
                                aukeratuListener(
                                    Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK,
                                    null,  // Hautatutakoak direnean ez da behar zerrenda pasatzerik klasean bertan dagoelako
                                    posizioa,
                                    hitza
                                )
                            },
                            { posizioa, hitza ->
                                ikusiErrimaListener(
                                    Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK,
                                    null,  // Hautatutakoak direnean ez da behar zerrenda pasatzerik klasean bertan dagoelako
                                    posizioa,
                                    hitza
                                )
                            }
                        )
                        binding.drawerLayout.rvHitzakHautatuak.adapter = hitzaAdapter
                    }

                    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                    }

                    override fun isLongPressDragEnabled(): Boolean {
                        return true
                    }

                    override fun isItemViewSwipeEnabled(): Boolean {
                        return false
                    }

                    override fun clearView(
                        recyclerView: RecyclerView,
                        viewHolder: RecyclerView.ViewHolder
                    ) {
                        super.clearView(recyclerView, viewHolder);

                        if(dragFrom != -1 && dragTo != -1 && dragFrom != dragTo) {

                            reallyMoved(dragFrom, dragTo);
                        }

                        dragFrom = -1
                        dragTo = -1
                    }
                }
            )
            Log.d(this::class.simpleName, "setupHautatutakoakRecyclerView() - helper.attachToRecyclerView()")
            helper.attachToRecyclerView(binding.drawerLayout.rvHitzakHautatuak)
        }
    }

    fun aukeratuListener(
        zerrendaMota: String,
        hitzZerrenda: ArrayList<HitzaZerrendan>?,  // zerrendaMota hautatutakoak denean, hau null da
        posizioa: Int,
        hitza: HitzaZerrendan
    ){
        joSoinua(this, R.raw.soinua_tip)

        if (zerrendaMota == Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK) {
            // Hautatutako hitzen zerrendako hitz batean egin du klik
            if (!hautatutakoHitzenZerrenda.hautatutakoHitzak[posizioa].hutsikDago()){
                // Errimaren/Familiakoen hitzetan gaitu
                if (hautatutakoHitzenZerrenda.hautatutakoHitzak[posizioa].zerrendaMota ==
                    Konstanteak.BILATU_ERRIMAK_ZERRENDA_ERRIMARENAK){
                    // Errimaren hitzetan gaitu
                    for (i in errimarenHitzenZerrenda.indices){
                        if (errimarenHitzenZerrenda[i].hitza == hautatutakoHitzenZerrenda.hautatutakoHitzak[posizioa].hitza){
                            errimarenHitzenZerrenda[i].desgaitutaDago = false
                            break
                        }
                    }

                    val hitzaAdapter = HitzaAdapter(
                        Konstanteak.BILATU_ERRIMAK_ZERRENDA_ERRIMARENAK,
                        errimarenHitzenZerrenda,
                        { posizioa, hitza ->
                            aukeratuListener(
                                Konstanteak.BILATU_ERRIMAK_ZERRENDA_ERRIMARENAK,
                                errimarenHitzenZerrenda,
                                posizioa,
                                hitza
                            )
                        },
                        { posizioa, hitza ->
                            ikusiErrimaListener(
                                Konstanteak.BILATU_ERRIMAK_ZERRENDA_ERRIMARENAK,
                                errimarenHitzenZerrenda,
                                posizioa,
                                hitza
                            )
                        }
                    )
                    binding.drawerLayout.rvHitzakErrimarenak.layoutManager = LinearLayoutManager(this)
                    Log.d(this::class.simpleName, "aukeratuListener() - rvHitzakErrimarenak hitzaAdapter")
                    binding.drawerLayout.rvHitzakErrimarenak.adapter = hitzaAdapter
                }else{
                    // Familiakoen hitzetan gaitu
                    for (i in familiakoenHitzenZerrenda.indices){
                        if (familiakoenHitzenZerrenda[i].hitza == hautatutakoHitzenZerrenda.hautatutakoHitzak[posizioa].hitza){
                            familiakoenHitzenZerrenda[i].desgaitutaDago = false
                            break
                        }
                    }

                    val hitzaAdapter = HitzaAdapter(
                        Konstanteak.BILATU_ERRIMAK_ZERRENDA_FAMILIAKOAK,
                        familiakoenHitzenZerrenda,
                        { posizioa, hitza ->
                            aukeratuListener(
                                Konstanteak.BILATU_ERRIMAK_ZERRENDA_FAMILIAKOAK,
                                familiakoenHitzenZerrenda,
                                posizioa,
                                hitza
                            )
                        },
                        { posizioa, hitza ->
                            ikusiErrimaListener(
                                Konstanteak.BILATU_ERRIMAK_ZERRENDA_FAMILIAKOAK,
                                familiakoenHitzenZerrenda,
                                posizioa,
                                hitza
                            )
                        }
                    )
                    binding.drawerLayout.rvHitzakFamiliakoak.layoutManager = LinearLayoutManager(this)
                    Log.d(this::class.simpleName, "aukeratuListener() - rvHitzakFamiliakoak hitzaAdapter")
                    binding.drawerLayout.rvHitzakFamiliakoak.adapter = hitzaAdapter
                }

                // Hautatutako hitzetan kendu
                hautatutakoHitzenZerrenda.kendu(posizioa)
                binding.drawerLayout.rvHitzakHautatuak.adapter?.notifyItemChanged(posizioa)
                setupHautatutakoakRecyclerView()
            }


        }else{
            // Errimaren/Familietako zerrendako hitz batean egin du klik
            if (hitzZerrenda != null){ // Errimarenak bada hitzZerrenda ez da inoiz nulua izango
                if (hautatutakoHitzenZerrenda.kopuruaHutsik() == 0){
                    bistaratuMezua(
                        getString(R.string.bilatu_errimak_mezua_hautatutakoak_beteta),
                        Konstanteak.MEZU_MOTA_INFORMAZIOA
                    )
                }else{

                    // Errimaren/Familiakoen hitzetan desgaitu
                    if (zerrendaMota == Konstanteak.BILATU_ERRIMAK_ZERRENDA_ERRIMARENAK){
                        // Errimaren hitzetan desgaitu
                        errimarenHitzenZerrenda[posizioa].zerrendaMota = zerrendaMota
                        if (!errimarenHitzenZerrenda[posizioa].polisemikoaDa){
                            errimarenHitzenZerrenda[posizioa].desgaitutaDago = true
                        }
                        val hitzaAdapter = HitzaAdapter(
                            zerrendaMota,
                            errimarenHitzenZerrenda,
                            { posizioa, hitza ->
                                aukeratuListener(
                                    zerrendaMota,
                                    errimarenHitzenZerrenda,
                                    posizioa,
                                    hitza
                                )
                            },
                            { posizioa, hitza ->
                                ikusiErrimaListener(
                                    zerrendaMota,
                                    errimarenHitzenZerrenda,
                                    posizioa,
                                    hitza
                                )
                            }
                        )
                        binding.drawerLayout.rvHitzakErrimarenak.layoutManager = LinearLayoutManager(this)
                        Log.d(this::class.simpleName, "aukeratuListener()- rvHitzakErrimarenak hitzaAdapter")
                        binding.drawerLayout.rvHitzakErrimarenak.adapter = hitzaAdapter

                        // Hautatutakoen hitzetan gehitu
                        hautatutakoHitzenZerrenda.gehitu(errimarenHitzenZerrenda[posizioa])  // TODO fokoan duen lekuan gehitu
                        binding.drawerLayout.rvHitzakHautatuak.adapter?.notifyItemChanged(posizioa)
                        setupHautatutakoakRecyclerView()
                    }else{
                        // Familiakoen hitzetan desgaitu
                        familiakoenHitzenZerrenda[posizioa].zerrendaMota = zerrendaMota
                        if (!familiakoenHitzenZerrenda[posizioa].polisemikoaDa){
                            familiakoenHitzenZerrenda[posizioa].desgaitutaDago = true
                        }
                        val hitzaAdapter = HitzaAdapter(
                            zerrendaMota,
                            familiakoenHitzenZerrenda,
                            { posizioa, hitza ->
                                aukeratuListener(
                                    zerrendaMota,
                                    familiakoenHitzenZerrenda,
                                    posizioa,
                                    hitza
                                )
                            },
                            { posizioa, hitza ->
                                ikusiErrimaListener(
                                    zerrendaMota,
                                    familiakoenHitzenZerrenda,
                                    posizioa,
                                    hitza
                                )
                            }
                        )
                        binding.drawerLayout.rvHitzakFamiliakoak.layoutManager = LinearLayoutManager(this)
                        Log.d(this::class.simpleName, "aukeratuListener() - rvHitzakFamiliakoak hitzaAdapter")
                        binding.drawerLayout.rvHitzakFamiliakoak.adapter = hitzaAdapter

                        // Hautatutakoen hitzetan gehitu
                        hautatutakoHitzenZerrenda.gehitu(familiakoenHitzenZerrenda[posizioa])  // TODO fokoan duen lekuan gehitu
                        binding.drawerLayout.rvHitzakHautatuak.adapter?.notifyItemChanged(posizioa)
                        setupHautatutakoakRecyclerView()
                    }
                }
            }
        }
    }

    fun hautatutakoAzkenaBilatutakoaDa(): Boolean{
        var azkenaBilatutakoaDa = false
        if (bilatutakoHitzaString.isNotEmpty() &&
            hautatutakoHitzenZerrenda.hautatutakoHitzak.isNotEmpty() &&
            !hautatutakoHitzenZerrenda.hautatutakoHitzak[hautatutakoHitzenZerrenda.kopurua()-1].hutsikDago() &&
            hautatutakoHitzenZerrenda.hautatutakoHitzak[hautatutakoHitzenZerrenda.kopurua()-1].lortuHitza() == bilatutakoHitzaString
        ){
            azkenaBilatutakoaDa = true
        }
        return azkenaBilatutakoaDa
    }

    fun aldatuErrimaKopurua(){
        val aldatuErrimaKopuruaDialogoa = Dialog(this, androidx.appcompat.R.style.Theme_AppCompat_Dialog_MinWidth)
        //aldatuErrimaKopuruaDialogoa.setCancelable(false)  // Kanpoan klik eginez gero ixterik nahi ez badugu, hau deskomentatu
        var kopuruaOrain = hautatutakoHitzenZerrenda.kopurua()
        val kopuruaSartzean = kopuruaOrain
        var kopuruBerria = kopuruaOrain

        val bindingDialogoa = DialogErrimaKopuruaBinding.inflate(layoutInflater)
        aldatuErrimaKopuruaDialogoa.setContentView(bindingDialogoa.root)

        bindingDialogoa.rgKopurua.setOnCheckedChangeListener { radioGroup, checkedId ->
            var aukeratutakoId = 1
            if (checkedId != null){
                aukeratutakoId = checkedId
            }
            val radioButton = radioGroup.findViewById(aukeratutakoId) as RadioButton
            kopuruBerria = radioButton.text.toString().toInt()
            bindingDialogoa.tvAukeratutakoaKopurua.text = when (kopuruBerria){
                2 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_2_aukera)
                3 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_3_aukera)
                4 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_4_aukera)
                5 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_5_aukera)
                6 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_6_aukera)
                8 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_8_aukera)
                else -> ""
            }
            bindingDialogoa.tvAukeratutakoaAzalpena.text = when (kopuruBerria){
                2 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_2_azalpena)
                3 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_3_azalpena)
                4 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_4_azalpena)
                5 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_5_azalpena)
                6 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_6_azalpena)
                8 -> getString(R.string.bilatu_errimak_dialog_errima_kopurua_8_azalpena)
                else -> ""
            }
        }
        when (kopuruBerria){
            2 -> bindingDialogoa.rbKopurua2.isChecked = true
            3 -> bindingDialogoa.rbKopurua3.isChecked = true
            4 -> bindingDialogoa.rbKopurua4.isChecked = true
            5 -> bindingDialogoa.rbKopurua5.isChecked = true
            6 -> bindingDialogoa.rbKopurua6.isChecked = true
            8 -> bindingDialogoa.rbKopurua8.isChecked = true
        }

        bindingDialogoa.btnAdos.setOnClickListener {
            if (kopuruBerria != kopuruaSartzean){
                Log.d(this::class.simpleName, "Hautatutakoen zerrendak $kopuruaOrain hitz ditu eta $kopuruBerria hitz izan behar ditu")
                if (kopuruBerria > kopuruaSartzean){
                    // Zenbat hitz txertatu behar dira?
                    var zenbatTxertatu = kopuruBerria - kopuruaSartzean
                    //Log.d(this::class.simpleName, "Hautatutakoen zerrendan $zenbatTxertatu hitz txertatu behar dira")

                    // Azkenari tratamendu berezia egin beharko zaion ala ez argitu
                    var azkenaBilatutakoaDa = hautatutakoAzkenaBilatutakoaDa()
                    //Log.d(this::class.simpleName, "azkenaBilatutakoaDa: $azkenaBilatutakoaDa")

                    // Hitz huts batzuk gehitu azkenaurreko posizioetan
                    do{
                        //Log.d(this::class.simpleName, "Hautatutakoen zerrendako ${hautatutakoHitzenZerrenda.kopurua() - 1}. posizioan hitz hutsa txertatzen")
                        if (azkenaBilatutakoaDa){
                            hautatutakoHitzenZerrenda.txertatu(hautatutakoHitzenZerrenda.kopurua() - 1)
                        }else{
                            hautatutakoHitzenZerrenda.txertatu()
                        }

                        zenbatTxertatu--
                    }while (zenbatTxertatu > 0)
                }else{
                    // Zenbat hitz atera behar dira?
                    var zenbatAtera = kopuruaSartzean - kopuruBerria
                    //Log.d(this::class.simpleName, "Hautatutakoen zerrendatik $zenbatAtera hitz atera behar dira")

                    // Azkenari tratamendu berezia egin beharko zaion ala ez argitu
                    var azkenaBilatutakoaDa = hautatutakoAzkenaBilatutakoaDa()
                    //Log.d(this::class.simpleName, "azkenaBilatutakoaDa: $azkenaBilatutakoaDa")

                    // Lehenik eta behin hutsik daudenak kendu atzekoz aurrera
                    var zenbatHutsAtera = min(zenbatAtera, hautatutakoHitzenZerrenda.kopuruaHutsik())
                    for (i in hautatutakoHitzenZerrenda.kopurua() -1 downTo 0){
                        if (hautatutakoHitzenZerrenda.hautatutakoHitzak[i].hutsikDago()){
                            //Log.d(this::class.simpleName, "Hautatutakoen zerrendatik $i. hitz hutsa ateratzen")
                            hautatutakoHitzenZerrenda.atera(i)
                            zenbatHutsAtera--
                            if (zenbatHutsAtera == 0)
                                break
                        }
                    }

                    if (hautatutakoHitzenZerrenda.kopurua() > kopuruBerria){
                        // Gehiago kendu behar badira azkenetik edo azkenaurrekotik hasi atzekoz aurrera
                        var zenbatBeteAtera = hautatutakoHitzenZerrenda.kopurua() - kopuruBerria
                        //Log.d(this::class.simpleName, "Hautatutakoen zerrendatik $zenbatBeteAtera hitz ez huts atera behar dira")
                        var nondikHasi = hautatutakoHitzenZerrenda.kopurua() - 1
                        //Log.d(this::class.simpleName, "nondikHasi: $nondikHasi")
                        if (azkenaBilatutakoaDa)
                            nondikHasi--
                        for (i in nondikHasi downTo 0){
                            // Lehenik eta behin hitz hori desaukeratu
                            if(!hautatutakoHitzenZerrenda.hautatutakoHitzak[i].hutsikDago()){
                                //Log.d(this::class.simpleName, "Hautatutakoen zerrendako $i. hitza desaukeratzen: ${hautatutakoHitzenZerrenda.hautatutakoHitzak[i].lortuHitza()}")
                                aukeratuListener(
                                    Konstanteak.BILATU_ERRIMAK_ZERRENDA_HAUTATUAK,
                                    hautatutakoHitzenZerrenda.hautatutakoHitzak,
                                    i,
                                    hautatutakoHitzenZerrenda.hautatutakoHitzak[i]
                                )
                            }
                            // Eta ondoren atera zerrendatik
                            //Log.d(this::class.simpleName, "Hautatutakoen zerrendatik $i. hitza ateratzen")
                            hautatutakoHitzenZerrenda.atera(i)

                            zenbatBeteAtera--
                            if (zenbatBeteAtera == 0)
                                break
                        }
                    }

                    Log.d(this::class.simpleName, "Hautatutakoen zerrendak orain ${hautatutakoHitzenZerrenda.kopurua()} hitz ditu")
                }
                binding.drawerLayout.rvHitzakHautatuak.removeAllViewsInLayout()
                setupHautatutakoakRecyclerView()
            }
            aldatuErrimaKopuruaDialogoa.dismiss()
        }

        aldatuErrimaKopuruaDialogoa.show()
    }

    fun ikusiHautatutakoErrimak(){
        ikusiHautatutakoErrimakDialogoa?.let{
            val binding = DialogIkusiHautatutakoErrimakBinding.inflate(layoutInflater)
            ikusiHautatutakoErrimakDialogoa!!.setContentView(binding.root)

            var errimak = ArrayList<HitzaZerrendan>()
            for (i in hautatutakoHitzenZerrenda.hautatutakoHitzak.indices){
                if(!hautatutakoHitzenZerrenda.hautatutakoHitzak[i].hutsikDago()){
                    errimak.add(hautatutakoHitzenZerrenda.hautatutakoHitzak[i])
                }
            }
            if (errimak.size > 0){
                joSoinua(this@BilatuErrimakActivity.baseContext, R.raw.soinua_tititin)

                binding.clMain.setOnClickListener {
                    ikusiHautatutakoErrimakDialogoa!!.dismiss()
                }
                binding.rvErrimak.setOnClickListener {
                    ikusiHautatutakoErrimakDialogoa!!.dismiss()
                }

                val ikusiErrimakAdapter = IkusiErrimakAdapter(
                    errimak,
                    {-> itxiHautatutakoErrimakListener()}
                )
                binding.rvErrimak.layoutManager = LinearLayoutManager(this)
                binding.rvErrimak.adapter = ikusiErrimakAdapter

                ikusiHautatutakoErrimakDialogoa!!.show()
            }
        }
    }

    fun itxiHautatutakoErrimakListener(){
        ikusiHautatutakoErrimakDialogoa?.let{
            ikusiHautatutakoErrimakDialogoa!!.dismiss()
        }
    }

    fun kopiatuErrimak(){
        // Kopiatu beharreko testua prestatu
        var testua: String = ""
        if (hautatutakoHitzenZerrenda.kopuruaBeteta() == 1){
            testua = getString(R.string.bilatu_errimak_kopiatu_memoria_bat)
        }else{
            testua = getString(R.string.bilatu_errimak_kopiatu_memoria_batzuk)
        }
        testua += " "
        for (hautatutakoHitza in hautatutakoHitzenZerrenda.hautatutakoHitzak){
            if (!hautatutakoHitza.hutsikDago()){
                testua += hautatutakoHitza.lortuHitza()
                testua += ", "
            }
        }
        // Amaieran gehitu berri den azken ", " hori kendu (2 hizki) eta puntu batengatik ordeztu
        testua = testua.substring(0, testua.length - 2)
        testua += "."

        // Testua memoriara kopiatu
        val myClipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val myClip = ClipData.newPlainText("text", testua)
        myClipboard.setPrimaryClip(myClip)

        // Mezua bistaratu
        var mezua: String = ""
        if (hautatutakoHitzenZerrenda.kopuruaBeteta() == 1){
            mezua = getString(R.string.bilatu_errimak_kopiatu_mezua_bat)
        }else{
            mezua = getString(R.string.bilatu_errimak_kopiatu_mezua_batzuk)
        }
        bistaratuMezua(
            mezua,
            Konstanteak.MEZU_MOTA_ARRAKASTA
        )
    }

    fun ikusiErrimaListener(
        zerrendaMota: String,
        hitzZerrenda: ArrayList<HitzaZerrendan>?,  // zerrendaMota hautatutakoak denean, hau null da
        posizioa: Int,
        hitza: HitzaZerrendan
    ){
        val ikusiErrimaDialogoa = Dialog(this, androidx.appcompat.R.style.Theme_AppCompat_Dialog_MinWidth)
        //ikusiErrimaDialogoa.setCancelable(false)  // Kanpoan klik eginez gero ixterik nahi ez badugu, hau deskomentatu

        val binding = DialogIkusiErrimaBinding.inflate(layoutInflater)
        ikusiErrimaDialogoa.setContentView(binding.root)

        binding.tvErrima.text = hitza.lortuHitza()
        var ahoskera1 = hitza.ahoskera1
        if (hitza.eskuzkoaDa){
            ahoskera1 = lortuAhoskera1(hitza.lortuHitza())
        }
        binding.tvAhoskera.text = "[$ahoskera1]"

        if(hitza.eskuzkoaDa){
            binding.llMarkak.visibility = View.GONE
        }else{
            binding.llMarkak.visibility = View.VISIBLE

            if (hitza.polisemikoaDa){
                gaituMarka(binding.chipPol)
            }else{
                desgaituMarka(binding.chipPol)
            }
            if (hitza.lokalaDa){
                gaituMarka(binding.chipLok)
            }else{
                desgaituMarka(binding.chipLok)
            }
            if (hitza.lagunartekoaDa){
                gaituMarka(binding.chipLag)
            }else{
                desgaituMarka(binding.chipLag)
            }
            if (hitza.erderakadaDa){
                gaituMarka(binding.chipErd)
            }else{
                desgaituMarka(binding.chipErd)
            }
            if (hitza.deklinatutaDago){
                gaituMarka(binding.chipDek)
            }else{
                desgaituMarka(binding.chipDek)
            }
        }

        if (hitza.iruzkina.isNullOrEmpty()){
            binding.llIruzkina.visibility = View.GONE
        }else{
            binding.llIruzkina.visibility = View.VISIBLE
            binding.tvIruzkina.text = hitza.iruzkina
        }

        binding.chipPol.setOnClickListener {
            /*bistaratuMezua(
                binding.llIruzkina.context,
                resources.getString(R.string.bilatu_errimak_dialog_ikusi_errima_pol_mezua),
                Konstanteak.MEZU_MOTA_INFORMAZIOA
            )*/ // TODO Toast baten ordez Snackbar edo antzeko beste sistema bat erabili
            Toast.makeText(this, R.string.bilatu_errimak_dialog_ikusi_errima_pol_mezua, Toast.LENGTH_LONG).show()
        }
        binding.chipLok.setOnClickListener {
            Toast.makeText(this, R.string.bilatu_errimak_dialog_ikusi_errima_lok_mezua, Toast.LENGTH_LONG).show()
        }
        binding.chipLag.setOnClickListener {
            Toast.makeText(this, R.string.bilatu_errimak_dialog_ikusi_errima_lag_mezua, Toast.LENGTH_LONG).show()
        }
        binding.chipErd.setOnClickListener {
            Toast.makeText(this, R.string.bilatu_errimak_dialog_ikusi_errima_erd_mezua, Toast.LENGTH_LONG).show()
        }
        binding.chipDek.setOnClickListener {
            Toast.makeText(this, R.string.bilatu_errimak_dialog_ikusi_errima_dek_mezua, Toast.LENGTH_LONG).show()
        }
        binding.btnElhuyar.setOnClickListener{
            val url = "$BILATU_ERRIMAK_URL_ELHUYAR${hitza.lortuHitza()}"
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }
        binding.btnEuskaltzaindia.setOnClickListener{
            val url = "$BILATU_ERRIMAK_URL_EUSKALTZAINDIA${hitza.lortuHitza()}"
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }
        binding.btnAdos.setOnClickListener {
            ikusiErrimaDialogoa.dismiss()
        }

        ikusiErrimaDialogoa.show()
    }

    fun gaituMarka(marka: Chip){
        marka.chipBackgroundColor = AppCompatResources.getColorStateList(this.baseContext, R.color.colorOnPrimaryDark)
        marka.chipStrokeWidth = Tresnak.lortuPixelakDptik (0, this)
        marka.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
    }

    fun desgaituMarka(marka: Chip){
        marka.chipBackgroundColor = AppCompatResources.getColorStateList(this.baseContext, R.color.colorBackground)
        marka.chipStrokeColor = AppCompatResources.getColorStateList(this.baseContext, R.color.colorOnPrimaryDisabled)
        marka.chipStrokeWidth = Tresnak.lortuPixelakDptik (1, this)
        marka.setTextColor(ContextCompat.getColor(this, R.color.colorOnPrimaryDisabled))
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            BAIMENA_RECORD_AUDIO_ESKARI_KODEA -> if (grantResults.isNotEmpty()) {
                val permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED
                if (permissionToRecord) {
                    Log.i(this::class.simpleName, "RECORD_AUDIO baimendu da")
                    bistaratuMezua(getString(R.string.baimena_record_audio_onartuta), MEZU_MOTA_ARRAKASTA)
                } else {
                    Log.i(this::class.simpleName, "RECORD_AUDIO baimena ukatu da")
                    bistaratuMezua(getString(R.string.baimena_record_audio_ukatuta), MEZU_MOTA_ERROREA)
                }
            }
        }
    }

    fun egiaztatuBaimenak(): Boolean {
        val result = ContextCompat.checkSelfPermission(applicationContext, RECORD_AUDIO)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun eskatuBaimenak() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(RECORD_AUDIO),
            BAIMENA_RECORD_AUDIO_ESKARI_KODEA
        )
    }

}
