package eus.ikusimakusi.bertsotan_android.activities

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.media.MediaPlayer
import android.os.Handler
import android.view.ContextThemeWrapper
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import eus.ikusimakusi.bertsotan_android.R
import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak
import eus.ikusimakusi.bertsotan_android.tresnak.Tresnak
import kotlinx.android.synthetic.main.dialog_aurrerabidea.*

open class OinarrizkoActivity: AppCompatActivity() {

    private var doubleBackToExitPressedOnce = false

    private lateinit var mProgressDialog: Dialog

    fun joSoinua(context: Context, resid: Int){
        if(Tresnak.ezarpenakSoinuaGaitutaDago()){
            val mediaPlayer = MediaPlayer.create(context, resid)
            mediaPlayer.start()
        }
    }

    fun bistaratuAurrerabideDialogoa(text: String) {
        runOnUiThread {
            mProgressDialog = Dialog(this)

            mProgressDialog.setContentView(R.layout.dialog_aurrerabidea)

            mProgressDialog.tvAurrerabidea.text = text

            mProgressDialog.show()
        }
    }

    fun ezkutatuAurrerabideDialogoa() {
        runOnUiThread {
            mProgressDialog.dismiss()
        }
    }

    fun ezkutatuTeklatua() {
        runOnUiThread {
            val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            // Find the currently focused view, so we can grab the correct window token from it.
            var view: View? = this.currentFocus
            // If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(this)
            }
            imm.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS // Erabiltzaileak eremua ukitzen badu, teklatua berriz ateratzen da
            )
            view.clearFocus()
        }
    }

    fun bistaratuMezua(testua: String, mota: String) {
        runOnUiThread {
            val ctw = ContextThemeWrapper(this, R.style.CustomSnackbarTheme)
            bistaratuMezua(ctw, testua, mota)
        }
    }

    private fun bistaratuMezua(context: Context, testua: String, mota: String) {
        runOnUiThread {
            val customSnackBar = Snackbar.make(
                context,
                findViewById(android.R.id.content),
                testua,
                Snackbar.LENGTH_LONG
            )
            val customSnackbarView = customSnackBar.view
            val customSnackbarText = customSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView

            val testuarenKolorea = ContextCompat.getColor(this, R.color.white)
            customSnackbarText.setTextColor(testuarenKolorea)

            val atzekoKolorea = when (mota){
                Konstanteak.MEZU_MOTA_INFORMAZIOA -> ContextCompat.getColor(this, R.color.mezu_mota_informazioa)
                Konstanteak.MEZU_MOTA_ABISUA -> ContextCompat.getColor(this, R.color.mezu_mota_abisua)
                Konstanteak.MEZU_MOTA_ERROREA -> ContextCompat.getColor(this, R.color.mezu_mota_errorea)
                Konstanteak.MEZU_MOTA_ARRAKASTA -> ContextCompat.getColor(this, R.color.mezu_mota_arrakasta)
                else -> R.color.mezu_mota_errorea
            }
            customSnackbarView.setBackgroundColor(atzekoKolorea)

            customSnackBar.show()
        }
    }

    fun birritanAtzeraIrtetzeko() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        bistaratuMezua(
            resources.getString(R.string.mezua_birritan_irtetzeko),
            Konstanteak.MEZU_MOTA_INFORMAZIOA
        )

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
}