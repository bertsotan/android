package eus.ikusimakusi.bertsotan_android.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.google.gson.Gson
import eus.ikusimakusi.bertsotan_android.ErrimategiaApp
import eus.ikusimakusi.bertsotan_android.R
import eus.ikusimakusi.bertsotan_android.database.BukaeraEntity
import eus.ikusimakusi.bertsotan_android.database.HitzaEntity
import eus.ikusimakusi.bertsotan_android.database.PropietateaEntity
import eus.ikusimakusi.bertsotan_android.databinding.ActivitySartuBinding
import eus.ikusimakusi.bertsotan_android.models.Errimategia
import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak
import eus.ikusimakusi.bertsotan_android.tresnak.Tresnak
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream

class SartuActivity : OinarrizkoActivity() {

    private lateinit var layout: View

    private lateinit var binding: ActivitySartuBinding

    private var errorea: Boolean = false

    private val hideHandler = Handler(Looper.myLooper()!!)

    /*
    @SuppressLint("InlinedApi")
    private val hidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar
        if (Build.VERSION.SDK_INT >= 30) {
            fullscreenContent.windowInsetsController?.hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
        } else {
            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            fullscreenContent.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LOW_PROFILE or
                        View.SYSTEM_UI_FLAG_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
    }
    private val showPart2Runnable = Runnable {
        // Delayed display of UI elements
        supportActionBar?.show()
        fullscreenContentControls.visibility = View.VISIBLE
    }
    */

    private val hideRunnable = Runnable { hide() }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private val delayHideTouchListener = View.OnTouchListener { view, motionEvent ->
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS)
            }
            MotionEvent.ACTION_UP -> view.performClick()
            else -> {
            }
        }
        false
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySartuBinding.inflate(layoutInflater)
        setContentView(binding.root)
        layout = binding.root

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if(Tresnak.ezarpenakAsrGaitutaDago()){
            binding.clAsr.visibility = View.VISIBLE
        }else{
            binding.clAsr.visibility = View.GONE
        }

        if (!errorea){
            lifecycleScope.launch {
                errimategiaEtaAsrPrestatu()
            }
        }
    }


    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100)
    }

    private fun hide() {
        // Hide UI first
        supportActionBar?.hide()

        // Schedule a runnable to remove the status and navigation bar after a delay
        //hideHandler.removeCallbacks(showPart2Runnable)
        //hideHandler.postDelayed(hidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    private fun show() {
        // Show the system bar
        /*
        if (Build.VERSION.SDK_INT >= 30) {
            fullscreenContent.windowInsetsController?.show(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
        } else {
            fullscreenContent.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        }
        isFullscreen = true

        // Schedule a runnable to display UI elements after a delay
        hideHandler.removeCallbacks(hidePart2Runnable)
        hideHandler.postDelayed(showPart2Runnable, UI_ANIMATION_DELAY.toLong())
        */
    }

    /**
     * Schedules a call to hide() in [delayMillis], canceling any
     * previously scheduled calls.
     */
    private fun delayedHide(delayMillis: Int) {
        hideHandler.removeCallbacks(hideRunnable)
        hideHandler.postDelayed(hideRunnable, delayMillis.toLong())
    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private const val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private const val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private const val UI_ANIMATION_DELAY = 300
    }

    private suspend fun extractSttFile(fromInputStream: InputStream, toFile: File){
        // fromInputStream:
        //   storage/emulated/0/Android/data/eus.ikusimakusi.bertsotan_android/files/huge_vocab.scorer
        //   storage/emulated/0/Android/data/eus.ikusimakusi.bertsotan_android/files/model.tflite
        try {
            Log.d(this::class.simpleName, "Nora: " + toFile.absolutePath)
            fromInputStream.use { fis ->
                FileOutputStream(toFile).use { os ->
                    val buffer = ByteArray(1024)
                    var len: Int
                    while (fis.read(buffer).also { len = it } != -1) {
                        os.write(buffer, 0, len)
                    }
                    os.close()
                }
                fis.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    private suspend fun createDeviceFiles() {
        withContext(Dispatchers.IO) {
            try {
                // Create application data directory on the device
                val modelsPath = getExternalFilesDir(null).toString()

                // Copy STT model file from app resources to devices internal storage
                var fromInputStream: InputStream = resources.openRawResource(R.raw.model)
                var toFile = File(modelsPath + File.separator + Konstanteak.ASR_TFLITE_IZENA)
                extractSttFile(fromInputStream, toFile) // TODO ez du fitxategia kopiatzen!

                // Copy Scored file from app resources to devices internal storage
                fromInputStream = resources.openRawResource(R.raw.kenlm)
                toFile = File(modelsPath + File.separator + Konstanteak.ASR_SCORER_IZENA)
                extractSttFile(fromInputStream, toFile) // TODO ez du fitxategia kopiatzen!

                Log.i(this@SartuActivity::class.simpleName, resources.getText(R.string.sartu_asr_prest).toString())
                runOnUiThread {
                    binding.ivAsr.setBackgroundResource(R.drawable.ikonoa_check_circle_24)
                    binding.tvAsr.text = resources.getText(R.string.sartu_asr_prest)
                    startActivity(Intent(this@SartuActivity, BilatuErrimakActivity::class.java))
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d(this@SartuActivity::class.simpleName, resources.getText(R.string.sartu_asr_errorea).toString())
                binding.tvAsr.text = resources.getText(R.string.sartu_asr_errorea)
                binding.btnIrten.visibility = View.VISIBLE
            }
        }
    }

    private suspend fun asrPrestatu() {
        withContext(Dispatchers.IO) {
            Log.i(this::class.simpleName, resources.getText(R.string.sartu_asr_prestatzen).toString())
            runOnUiThread {
                binding.ivAsr.visibility = View.VISIBLE
                binding.tvAsr.text = resources.getText(R.string.sartu_asr_prestatzen)
            }

            lifecycleScope.launch{
                createDeviceFiles()
            }
        }
    }

    private suspend fun lortuErrimategia(): Errimategia{
        var errimategiaString: String
        withContext(Dispatchers.IO) {
            errimategiaString = resources.openRawResource(R.raw.errimategia).bufferedReader().use {
                it.readText()
            }
        }
        //Log.d(this::class.simpleName, "errimategiaString.length: ${errimategiaString.length.toString()}")
        return Gson().fromJson(errimategiaString, Errimategia::class.java)
    }

    private suspend fun errimategiaEtaAsrPrestatu() {
        withContext(Dispatchers.IO) {
            Log.i(this::class.simpleName, resources.getText(R.string.sartu_errimategia_sortzen).toString())
            binding.tvErrimategia.text = resources.getText(R.string.sartu_errimategia_sortzen)

            // Fitxategiko errimategia lortu
            val fitxategikoErrimategia = lortuErrimategia()
            Log.d(this::class.simpleName, "Fitxategiko errimategia. Formatua: ${fitxategikoErrimategia.formatua}")
            Log.d(this::class.simpleName, "Fitxategiko errimategia. Data: ${fitxategikoErrimategia.data}")
            Log.d(this::class.simpleName, "Fitxategiko errimategia. Gai kopurua: ${fitxategikoErrimategia.gaiak.size}")
            Log.d(this::class.simpleName, "Fitxategiko errimategia. Bukaera kopurua: ${fitxategikoErrimategia.bukaerak.size}")
            Log.d(this::class.simpleName, "Fitxategiko errimategia. Hitz kopurua: ${fitxategikoErrimategia.hitzak.size}")

            // Datu-basea dagoeneko prest dagoen ala ez egiaztatu
            val propietateaDao = (application as ErrimategiaApp).db.propietateaDao()
            val datubasearenFormatuaJarioa = propietateaDao.fetchPropietateaByIzena("formatua")
            var datubasearenFormatua = PropietateaEntity()
            var lehenAldiaDa = true
            CoroutineScope(Dispatchers.IO).launch {
                datubasearenFormatuaJarioa.collectLatest {
                    if (lehenAldiaDa){
                        lehenAldiaDa = false
                        if (it.isNotEmpty()){
                            datubasearenFormatua = it[0]
                        }
                        if (datubasearenFormatua.balioa != "") {
                            Log.d(this::class.simpleName, "Datubasean gordetako formatua: ${datubasearenFormatua.balioa}")
                            if (datubasearenFormatua.balioa != fitxategikoErrimategia.formatua) {
                                Log.i(this::class.simpleName, "Aplikazioak dakarren fitxategiko formatua ez da datu-basean gordetako berdina.")
                                errimategiaBerregin(fitxategikoErrimategia)
                            }else{
                                Log.i(this::class.simpleName, "Datu-baseko eta fitxategiko formatuak berdinak dira.")
                                // TODO data ere berdina den egiaztatu
                            }
                        } else {
                            Log.i(this::class.simpleName, "Datu-basean oraindik ez dago errimategirik.")
                            errimategiaBerregin(fitxategikoErrimategia)
                        }

                        Log.i(this::class.simpleName, resources.getText(R.string.sartu_errimategia_prest).toString())
                        runOnUiThread {
                            binding.ivErrimategia.setBackgroundResource(R.drawable.ikonoa_check_circle_24)
                            binding.tvErrimategia.text = resources.getText(R.string.sartu_errimategia_prest)
                        }

                        // ASR prestatu
                        if (Tresnak.ezarpenakAsrGaitutaDago()) {
                            asrPrestatu()
                        }else{
                            runOnUiThread {
                                startActivity(Intent(this@SartuActivity, BilatuErrimakActivity::class.java))
                                finish()
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun errimategiaBerregin(fitxategikoErrimategia: Errimategia) {
        // Errimategia hutsitu
        Log.i(this::class.simpleName, "Errimategia husten...")
        CoroutineScope(Dispatchers.IO).launch {
            (application as ErrimategiaApp).db.clearAllTables()
        }

        // Errimategia bete
        Log.i(this::class.simpleName, "Errimategia betetzen...")

        // Propietateak
        val propietateaDao = (application as ErrimategiaApp).db.propietateaDao()
        val fitxategikoFormatua = fitxategikoErrimategia.formatua // TODO
        Log.d(this::class.simpleName, "Fitxategiko formatua: $fitxategikoFormatua")
        val datubasearenFormatua = PropietateaEntity()
        datubasearenFormatua.propietatea = "formatua"
        datubasearenFormatua.balioa = fitxategikoFormatua
        CoroutineScope(Dispatchers.IO).launch {
            propietateaDao.insert(datubasearenFormatua)
        }
        val fitxategikoData = fitxategikoErrimategia.data
        Log.d(this::class.simpleName, "Fitxategiko data: $fitxategikoData")
        val datubasearenData =  PropietateaEntity()
        datubasearenData.propietatea = "data"
        datubasearenData.balioa = fitxategikoData
        CoroutineScope(Dispatchers.IO).launch {
            propietateaDao.insert(datubasearenData)
        }

        // Gaiak
        // TODO

        // Bukaerak
        val bukaeraZerrenda = fitxategikoErrimategia.bukaerak
        val bukaeraDao = (application as ErrimategiaApp).db.bukaeraDao()
        for (bukaera in bukaeraZerrenda) {
            val datubasekoBukaera = BukaeraEntity()
            datubasekoBukaera.ahoskera1 = bukaera.ahoskera1
            datubasekoBukaera.ahoskera2 = bukaera.ahoskera2

            CoroutineScope(Dispatchers.IO).launch {
                bukaeraDao.insert(datubasekoBukaera)
            }
            //Log.d(this::class.simpleName, "Bukaera hau gehitu da: ${bukaera.ahoskera1}")
        }

        // Hitzak
        val hitzZerrenda = fitxategikoErrimategia.hitzak
        val hitzaDao = (application as ErrimategiaApp).db.hitzaDao()
        for (hitza in hitzZerrenda) {
            val datubasekoHitza = HitzaEntity()
            datubasekoHitza.hitza = hitza.hitza
            datubasekoHitza.maiuskularikDu = hitza.maiuskularikDu
            datubasekoHitza.ahoskera1 = hitza.ahoskera1
            datubasekoHitza.ahoskera2 = hitza.ahoskera2
            datubasekoHitza.bukaera = hitza.bukaera
            datubasekoHitza.erabilgarritasuna = hitza.erabilgarritasuna
            datubasekoHitza.polisemikoaDa = hitza.polisemikoaDa
            datubasekoHitza.lokalaDa = hitza.lokalaDa
            datubasekoHitza.lagunartekoaDa = hitza.lagunartekoaDa
            datubasekoHitza.erderakadaDa = hitza.erderakadaDa
            datubasekoHitza.deklinatutaDago = hitza.deklinatutaDago
            if (hitza.iruzkina != null){
                datubasekoHitza.iruzkina = hitza.iruzkina
            }

            CoroutineScope(Dispatchers.IO).launch {
                hitzaDao.insert(datubasekoHitza)
            }
            //Log.d(this::class.simpleName, "Hitz hau gehitu da: ${hitza.hitza}")
        }
    }
}
