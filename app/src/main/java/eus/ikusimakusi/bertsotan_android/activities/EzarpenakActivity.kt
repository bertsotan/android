package eus.ikusimakusi.bertsotan_android.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import com.google.android.material.navigation.NavigationView
import eus.ikusimakusi.bertsotan_android.R
import eus.ikusimakusi.bertsotan_android.databinding.ActivityEzarpenakBinding
import eus.ikusimakusi.bertsotan_android.databinding.DialogAdosBinding
import eus.ikusimakusi.bertsotan_android.tresnak.Tresnak
import kotlinx.android.synthetic.main.activity_ezarpenak.*
import kotlinx.android.synthetic.main.app_bar_ezarpenak.*
import kotlinx.android.synthetic.main.app_bar_ezarpenak.view.*
import kotlinx.android.synthetic.main.content_ezarpenak.view.*

class EzarpenakActivity: OinarrizkoActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityEzarpenakBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEzarpenakBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupActionBar()

        nav_view.setNavigationItemSelectedListener(this)

        binding.drawerLayout.swAsr.isChecked = Tresnak.ezarpenakAsrGaitutaDago()
        binding.drawerLayout.swSoinua.isChecked = Tresnak.ezarpenakSoinuaGaitutaDago()

        binding.drawerLayout.ivHoniBuruz.setOnClickListener {
            val intent = Intent(this, HoniBuruzActivity::class.java)
            startActivity(intent)
        }
        binding.drawerLayout.tvHoniBuruz.setOnClickListener {
            val intent = Intent(this, HoniBuruzActivity::class.java)
            startActivity(intent)
        }
        binding.drawerLayout.swSoinua.setOnCheckedChangeListener { _, isChecked ->
            Tresnak.ezarriEzarpenakSoinua(isChecked)
        }
        binding.drawerLayout.swAsr.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked){
                bistaratuBerrabiaraziDialogoa()
            }else{
                Tresnak.ezarriEzarpenakAsr(false)
            }
        }
    }

    private fun setupActionBar(){
        setSupportActionBar(toolbar_ezarpenak)
        toolbar_ezarpenak.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
        toolbar_ezarpenak.setNavigationIcon(R.drawable.ikonoa_burgerra_24)

        toolbar_ezarpenak.setNavigationOnClickListener {
            toggleDrawer()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            toggleDrawer()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun toggleDrawer(){
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }else{
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }else{
            birritanAtzeraIrtetzeko()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.aukera_bilatu_errimak -> {
                val intent = Intent(this, BilatuErrimakActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
            R.id.aukera_ezarpenak -> {
                // Ez egin ezer, dagoeneko bertan gaudelako
            }
            R.id.aukera_irten -> {
                finishAffinity()
            }
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)

        return true
    }

    private fun bistaratuBerrabiaraziDialogoa() {
        val berrabiaraziDialogoa = Dialog(this, androidx.appcompat.R.style.Theme_AppCompat_Dialog_MinWidth)
        berrabiaraziDialogoa.setCancelable(false)  // Kanpoan klik eginez gero ez itxi dialogoa

        val bindingDialogoa = DialogAdosBinding.inflate(layoutInflater)
        berrabiaraziDialogoa.setContentView(bindingDialogoa.root)

        bindingDialogoa.tvMezua.text = resources.getString(R.string.dialogoa_ados_asr_berrabiarazi)

        bindingDialogoa.btnAdos.setOnClickListener {
            Tresnak.ezarriEzarpenakAsr(true)

            berrabiaraziDialogoa.dismiss()

            val intent = baseContext.packageManager.getLaunchIntentForPackage(baseContext.packageName)
            intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            finish()
            startActivity(intent)
        }

        berrabiaraziDialogoa.show()
    }

}