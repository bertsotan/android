package eus.ikusimakusi.bertsotan_android.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import eus.ikusimakusi.bertsotan_android.R
import eus.ikusimakusi.bertsotan_android.databinding.ActivityHoniBuruzBinding
import eus.ikusimakusi.bertsotan_android.tresnak.Konstanteak

class HoniBuruzActivity: OinarrizkoActivity() {

    private lateinit var binding: ActivityHoniBuruzBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHoniBuruzBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupActionBar()

        binding.tvBertsioa.text = getString(R.string.honi_buruz_bertsioa, " " + Konstanteak.APLIKAZIO_BERTSIO_ZENBAKIA)

        binding.tvData.text = Konstanteak.APLIKAZIO_ARGITARATZE_DATA

        binding.tvAplikazioa.setOnClickListener{
            val url = Konstanteak.HONI_BURUZ_URL_APLIKAZIOA
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }
        binding.tvErrimak.setOnClickListener{
            val url = Konstanteak.HONI_BURUZ_URL_ERRIMAK
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }
    }

    private fun setupActionBar(){
        setSupportActionBar(binding.tbHoniBuruz)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ikonoa_atzera_24)
        binding.tbHoniBuruz.setNavigationOnClickListener {
            finish()
        }
    }
}