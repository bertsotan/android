package eus.ikusimakusi.bertsotan_android

import android.app.Application
import eus.ikusimakusi.bertsotan_android.tresnak.Tresnak
import eus.ikusimakusi.bertsotan_android.database.ErrimategiaDatabase

class ErrimategiaApp: Application() {

    val db by lazy{
        ErrimategiaDatabase.getInstance(this)
    }

    override fun onCreate() {
        super.onCreate()
        Tresnak.init(this)
    }
}